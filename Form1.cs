﻿/**
 * TODO: Make two RTF consoles for CLient and Server.
 * Read all log to string. Compare every second. Show new log if not equal.
 
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;
using Microsoft.Win32;

// TODO: Implement sml save read from other project
namespace Mod_Starter
{
    public partial class Form1 : Form
    {
        string coreString;
        string libString;
        string gameString;
        string worldString;
        string missionString;
        bool watcherStarted = false;
        string thePreviousLastLine1 = "";
        string thePreviousLastLine2 = "";

        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Assembly thisAssem = typeof(Program).Assembly;
            AssemblyName thisAssemName = thisAssem.GetName();

            /*
            string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            using (Microsoft.Win32.RegistryKey akey = Registry.LocalMachine.OpenSubKey(registry_key))
            {
                foreach (string subkey_name in akey.GetSubKeyNames())
                {
                   // ConsoleLog.AppendText("\r\n"+subkey_name);
                    using (RegistryKey subkey = akey.OpenSubKey(subkey_name))
                    {
                        if (subkey.GetValue("DisplayName") != null)
                        {
                            ConsoleLog.AppendText("\r\n"+ subkey.GetValue("DisplayName").ToString());
                            Console.WriteLine(subkey.GetValue("DisplayName").ToString());
                        }
                    }
                }
            }
            */
            
           
            
            
            //toolTip1.SetToolTip(this.checkBox1, "My checkBox1");

            //Packer_AddonBuilder.Checked = Properties.Settings.Default.AddonBuilder;
            //Packer_MakePbo.Checked = Properties.Settings.Default.MakePbo;
            //Packer_PboProject.Checked = Properties.Settings.Default.PboProject;

            ToolsDir.Text = Properties.Settings.Default.ToolsDir;

            ConsoleLog.AppendText("\r\nAddonBuilder" + Properties.Settings.Default.AddonBuilder);
            ConsoleLog.AppendText("\r\nMakePbo" + Properties.Settings.Default.MakePbo);
            ConsoleLog.AppendText("\r\nPboProject" + Properties.Settings.Default.PboProject);


            Packer_AddonBuilder.Enabled = false;
            Packer_MakePbo.Enabled = false;
            Packer_PboProject.Enabled = false;

            if (File.Exists("util/MakePbo.exe"))
            {
                Packer_MakePbo.Enabled = true;  
            }
            else
            {
                ConsoleLog.AppendText("\r\n MakePbo.exe NOT FOUND");
            }

            if (File.Exists("util/pboProject.exe"))
            {
                Packer_PboProject.Enabled = true;
                
            } 
            else
            {
                ConsoleLog.AppendText("\r\n pboProject.exe NOT FOUND");
            }
            
            RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\WOW6432Node\bohemia interactive\dayz");
            //ConsoleLog.AppendText("\r\nRegistry key: "+key);
            //if it does exist, retrieve the stored values  
            if (key != null)
            {
                ConsoleLog.AppendText("\r\nDayZ folder autodetected: " + key.GetValue("main"));
                //Console.WriteLine(key.GetValue("Setting2"));
                DayZPath.Text = key.GetValue("main").ToString();
                key.Close();
            }
            
            RegistryKey tkey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Bohemia Interactive\Dayz Tools");
            //ConsoleLog.AppendText("\r\nRegistry key: "+key);
            //if it does exist, retrieve the stored values  
            if (tkey != null)
            {
                ConsoleLog.AppendText("\r\nDayZ Tools folder autodetected: " + tkey.GetValue("path"));
                //Console.WriteLine(key.GetValue("Setting2"));
                ToolsDir.Text = tkey.GetValue("path").ToString();
                tkey.Close();
            }

            if (Directory.Exists(DayZPath.Text.ToString()))
            {
                DayZPath.BackColor = Color.Lime;
               // StartLogWatchers();
            }
            else
            {
                ConsoleLog.AppendText(DayZPath.Text.ToString() + ": path not found");
                DayZPath.BackColor = Color.Red;
            }

            if (Directory.Exists(ToolsDir.Text.ToString()))
            {
                ToolsDir.BackColor = Color.Lime;
                Packer_AddonBuilder.Enabled = true;
                // StartLogWatchers();
            }
            else
            {
                ConsoleLog.AppendText(ToolsDir.Text.ToString() + ": path not found");
                ToolsDir.BackColor = Color.Red;
            }

            if (Directory.Exists(ServerDir.Text.ToString()))
            {
                ServerDir.BackColor = Color.Lime;
                // Packer_AddonBuilder.Enabled = true;
                // StartLogWatchers();
            }
            else
            {
                ConsoleLog.AppendText(ToolsDir.Text.ToString() + ": path not found");
                ServerDir.BackColor = Color.Red;
            }

            

            Version ver = thisAssemName.Version;
            createStructure.Enabled = false;
            CheckBoxPanel.Enabled = false;
            if (!File.Exists("scratchpad.txt"))
            {
                File.WriteAllText("scratchpad.txt", "A place to save your snippets or notes. Stored to scratchpad.txt near the Mod Starter.exe.");
            }

            this.Text = String.Format("Mod Starter {0}", ver);
            //modPfolder.Text = modPfolder.Text.ToString() + " ";
            modPfolder.Text = modPfolder.Text.Trim();

            if (IsProcessOpen("workbenchApp"))
            {
                StartWorkbench.Text = "STOP WORKBENCH";
                StartWorkbench.BackColor = Color.Maroon;
            }

            listDirs();
            listFiles();
            listPresets();
            
            checkAllStructure();





            if (isServerMod.Checked)
            {
                isServerMod.BackColor = Color.Orange;
            }
            else
            {
                isServerMod.BackColor = SystemColors.Window;
            }

            modPfolder.Text = modPfolder.Text.Trim();
            string fullPath = DiskPpath.Text + modPfolder.Text;
            Console.WriteLine("[modPfolder.Text]: " + modPfolder.Text.ToString());

            if (Directory.Exists(fullPath) && !String.IsNullOrEmpty(modPfolder.Text))
            {
                modPfolder.BackColor = Color.LimeGreen;
                checkAllStructure();
                createStructure.Enabled = false;
                openpFolder.Enabled = true;
                openPackFolder.Enabled = true;
                CheckBoxPanel.Enabled = false;
                packPbo.Enabled = true;
            }
            SetCurrentFolder(modPfolder.Text);

            WorkshopMods.Nodes.Add(TraverseDirectory());
            WorkshopMods.ExpandAll();
            //TreeviewWorkshopRestore(WorkshopMods.Nodes[0]);

            toolTip1.SetToolTip(this.WipeMissionDiag, "Deletes storage_1 and storage_0 at " + DayZPath.Text + "\\mpmissions");
            toolTip1.SetToolTip(this.infoIcon, "After you click the 'Create DayZ Server Bat' button,\r\nthis path will be used as the file destination.\r\nOtherwise, the bat will be put to 'util' folder.");

        }



        public bool IsProcessOpen(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName.Contains(name))
                {
                    return true;
                }
            }
            return false;
        }

        void TreeviewWorkshopRestore(TreeNode treeNode)
        {
            Console.WriteLine("SETTING NODES");
            string theLine = prefixMods.Text;
            theLine = theLine.Replace("!Workshop/@", "");
            string[] Items = theLine.Split(';');
            foreach (var item in Items)
            {
                Console.WriteLine(item.ToString());
            }
            //TreeNode TheNodes =  WorkshopMods.Nodes[0];
            foreach (System.Windows.Forms.TreeNode aNode in treeNode.Nodes)
            {


                int pos = Array.IndexOf(Items, aNode.Text);
                if (pos > -1)
                {
                    aNode.Checked = true;
                    Console.WriteLine(aNode.Text);
                }

            }
            Console.WriteLine("---------- STOP SETTING NODES");
        }

        private TreeNode TraverseDirectory()
        {

            string path = DayZPath.Text.ToString() + "\\!Workshop";
            TreeNode result = new TreeNode(path);
            /*
            foreach (var subdirectory in Directory.GetDirectories(path))
            {
                result.Nodes.Add(TraverseDirectory(subdirectory));
            }
            */

            Console.WriteLine("SETTING NODES");
            string theLine = prefixMods.Text;
            theLine = theLine.Replace("!Workshop/@", "");
            string[] Items = theLine.Split(';');
            if (Directory.Exists(path))
            {

                String[] dirs = System.IO.Directory.GetDirectories(path);

                int i;
                for (i = 0; i < dirs.Length; i++)
                {
                    Console.WriteLine(dirs[i].ToString());
                    //pDirectories.Items.Add(dirs[i].Replace(DiskPpath.Text.ToString(), ""));
                    string NodeText = dirs[i].Replace(path + "\\@", "");

                    int pos = Array.IndexOf(Items, NodeText);
                    if (pos > -1)
                    {
                        TreeNode theNode = result.Nodes.Add(NodeText);
                        theNode.Checked = true;
                    }
                }
                for (i = 0; i < dirs.Length; i++)
                {
                    Console.WriteLine(dirs[i].ToString());
                    //pDirectories.Items.Add(dirs[i].Replace(DiskPpath.Text.ToString(), ""));
                    string NodeText = dirs[i].Replace(path + "\\@", "");

                    int pos = Array.IndexOf(Items, NodeText);
                    if (pos == -1)
                    {
                        TreeNode theNode = result.Nodes.Add(NodeText);
                        //theNode.Checked = true;
                    }
                }

            }
            

            return result;
        }

        private void SetCurrentFolder(string searchString)
        {
            // Ensure we have a proper string to search for.
            if (!string.IsNullOrEmpty(searchString))
            {
                // Find the item in the list and store the index to the item.
                int index = pDirectories.FindString(searchString);
                // Determine if a valid index is returned. Select the item if it is valid.
                if (index != -1)
                    pDirectories.SetSelected(index, true);
                else
                    MessageBox.Show("The search string did not match any items in the ListBox: "+ searchString);
            }
         }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                ConfigModFolder.Text = folderBrowserDialog1.SelectedPath;
                string fullPath = folderBrowserDialog1.SelectedPath + "\\config_structure.xml";
                try
                {

                    if (!File.Exists(fullPath))
                    {
                        using (var tw = new StreamWriter(fullPath, true))
                        {

                            tw.WriteLine("<config>");
                            tw.WriteLine("<setting type=\"bool\" name=\"BoolVariable\" default=\"false\" />");
                            tw.WriteLine("</config>");
                            Console.WriteLine("Config file didn't exist. Created a default one.");
                            tw.Close();
                        }
                    }

                    XmlReader xmlFile = XmlReader.Create(folderBrowserDialog1.SelectedPath + "\\config_structure.xml", new XmlReaderSettings());
                    dataSet1.ReadXml(xmlFile);
                    dataGridView1.DataSource = dataSet1.Tables[0].DefaultView;
                    xmlFile.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);

                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                ConfigModFolder.Text = openFileDialog1.FileName;

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (BrowseP.ShowDialog() == DialogResult.OK)
            {
                DiskPpath.Text = BrowseP.SelectedPath;

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (BrowseDayZ.ShowDialog() == DialogResult.OK)
            {
                DayZPath.Text = BrowseDayZ.SelectedPath;

            }
        }

        private void DayZPath_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.DayZFolder = DayZPath.Text.ToString();
            Properties.Settings.Default.Save();
            if (Directory.Exists(DayZPath.Text.ToString()))
            {
                DayZPath.BackColor = Color.Lime;
                //StartLogWatchers();
            }
            else
            {
                ConsoleLog.AppendText(DayZPath.Text.ToString() + ": path not found");
                DayZPath.BackColor = Color.Red;
            }
        }

 

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            /* DataGridViewRow row = (DataGridViewRow)yourDataGridView.Rows[0].Clone();
				row.Cells[0].Value = "XYZ";
				row.Cells[1].Value = 50.2;
				yourDataGridView.Rows.Add(row);
			*/
        }



        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (dataGridView1.Rows.Count != 0)
            {
                FileStream fs2 = new FileStream(ConfigModFolder.Text + "\\config_structure.xml", FileMode.Open, FileAccess.Write);

                try
                {
                    dataSet1.Tables[0].WriteXml(fs2);
                    fs2.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            string command = "\r\ntaskkill /f /im \"MakePbo.exe\"";
            command += "\r\ntaskkill /f /im \"pboProject.exe\"";
            command += "\r\ntaskkill /f /im \"AddonBuilder.exe\"";
            // command += "\r\npause";
            ExecuteCommand(command);
        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void ConfigModFolder_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void resetAllBackgrounds()
        {

        }

        private void checkAllStructure()
        {
            if (true)
            {
                checkPath("Folder", DiskPpath.Text + modPfolder.Text + "\\1_Core", CoreCheckbox);
                checkPath("Folder", DiskPpath.Text + modPfolder.Text + "\\3_Game", GameCheckbox);
                checkPath("Folder", DiskPpath.Text + modPfolder.Text + "\\4_World", WorldCheckbox);
                checkPath("Folder", DiskPpath.Text + modPfolder.Text + "\\5_Mission", MissionCheckbox);
                checkPath("Folder", DiskPpath.Text + modPfolder.Text + "\\Data", DataCheckbox);
                checkPath("Folder", DiskPpath.Text + modPfolder.Text + "\\GUI", GuiCheckbox);
                checkPath("Folder", DiskPpath.Text + modPfolder.Text + "\\languagecore", LanguagaCheckbox);

                string path = DiskPpath.Text + modPfolder.Text + "\\config.cpp";
                string parsedTitle = "";
                string parsedDescription = "";
                string parsedDeps = "";
                string parsedAuthor = "";
                string parsedServermod = "";

                if (checkPath("File", DiskPpath.Text + modPfolder.Text + "\\$PBOPREFIX$.txt", modcppCheckbox))
                {
                    //TODO: Save prefix from app to file.
                }

                if (checkPath("File", DiskPpath.Text + modPfolder.Text + "\\config.cpp", modcppCheckbox))
                {

                    try
                    {

                        string readText = File.ReadAllText(path);

                        if (readText.Contains("name ="))
                        {
                            parsedTitle = readText.Split(new string[] { "name = \"" }, StringSplitOptions.None)[1]
                            .Split('"')[0]
                            .Trim();
                        };


                        if (readText.Contains("description ="))
                        {
                            parsedDescription = readText.Split(new string[] { "description = \"" }, StringSplitOptions.None)[1]
                            .Split('"')[0]
                            .Trim();

                        };

                        if (readText.Contains("dependencies[] ="))
                        {
                            parsedDeps = readText.Split(new string[] { "dependencies[] = {" }, StringSplitOptions.None)[1]
                            .Split('}')[0]
                            .Trim();
                        };

                        if (readText.Contains("author ="))
                        {
                            parsedAuthor = readText.Split(new string[] { "author = \"" }, StringSplitOptions.None)[1]
                            .Split('"')[0]
                            .Trim();
                        };

                        if (readText.Contains("type ="))
                        {
                            parsedServermod = readText.Split(new string[] { "type = \"" }, StringSplitOptions.None)[1]
                            .Split('"')[0]
                            .Trim();
                        };

                        modName.Text = parsedTitle;
                        modDescription.Text = parsedDescription;
                        modAuthor.Text = parsedAuthor;
                        requiredAddons.Text = parsedDeps;
                        if (parsedServermod == "servermod")
                        {
                            isServerMod.Checked = true;
                        }
                        else
                        {
                            isServerMod.Checked = false;
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("{0} Exception caught.", e);
                    }
                    //Console.WriteLine(parsedDeps);
                }
                else
                {
                    modName.Text = "";
                    modDescription.Text = "";
                    modAuthor.Text = "";
                    requiredAddons.Text = "";
                }
                checkPath("Folder", DiskPpath.Text + "@" + modPfolder.Text, pboFolderCheckpbox);
                checkPath("File", DiskPpath.Text + "@" + modPfolder.Text + "\\mod.cpp", modcppCheckbox);
                checkPath("Folder", DiskPpath.Text + "@" + modPfolder.Text + "\\Types", TypesFolderCheckbox);
                textBox1.Text = File.ReadAllText("scratchpad.txt");
            }
        }
        public static String betweenStrings(String text, String start, String end)
        {
            int p1 = text.IndexOf(start) + start.Length;
            int p2 = text.IndexOf(end, p1);

            if (end == "") return (text.Substring(p1));
            else return text.Substring(p1, p2 - p1);
        }
        private bool checkPath(string mode, string path, Control theControl)
        {

            string fullPath = path;
            Console.WriteLine("[FULL PATH]: " + fullPath);
            bool pathExists = false;
            if (mode == "File") { pathExists = File.Exists(fullPath); }
            if (mode == "Folder") { pathExists = Directory.Exists(fullPath); }
            if (pathExists && DiskPpath.Text != fullPath.Trim())
            {
                theControl.BackColor = Color.LimeGreen;
                if (theControl is CheckBox)
                {
                    CheckBox theCheckbox = (CheckBox)theControl;
                    theCheckbox.Checked = true;
                }
            }
            else
            {
                theControl.BackColor = SystemColors.Window;

                if (theControl is CheckBox)
                {
                    CheckBox theCheckbox = (CheckBox)theControl;
                    theCheckbox.Checked = false;
                }

            }

            return pathExists;
        }

        private void createStructure_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            createPfolder(DiskPpath.Text + modPfolder.Text, true);
            createPfolder(DiskPpath.Text + modPfolder.Text + "\\Common", GameCheckbox.Checked);
            createPfolder(DiskPpath.Text + modPfolder.Text + "\\1_Core", GameCheckbox.Checked);
            createPfolder(DiskPpath.Text + modPfolder.Text + "\\2_GameLib", GameCheckbox.Checked);
            createPfolder(DiskPpath.Text + modPfolder.Text + "\\3_Game", GameCheckbox.Checked);
            createPfolder(DiskPpath.Text + modPfolder.Text + "\\4_World", WorldCheckbox.Checked);
            createPfolder(DiskPpath.Text + modPfolder.Text + "\\5_Mission", MissionCheckbox.Checked);
            createPfolder(DiskPpath.Text + modPfolder.Text + "\\Data", DataCheckbox.Checked);
            createPfolder(DiskPpath.Text + modPfolder.Text + "\\GUI", GuiCheckbox.Checked);
            createPfolder(DiskPpath.Text + modPfolder.Text + "\\languagecore", LanguagaCheckbox.Checked);

            createPfolder(DiskPpath.Text + "@" + modPfolder.Text, true);
            createPfolder(DiskPpath.Text + "@" + modPfolder.Text + "\\Addons", true);
            createPfile(DiskPpath.Text + "@" + modPfolder.Text + "\\mod.cpp", modcppCheckbox.Checked);
            createPfolder(DiskPpath.Text + "@" + modPfolder.Text + "\\Types", TypesFolderCheckbox.Checked);

            //Create Config Lib

            //create file
            string configLibContents = String.Format(@"

class {0}_CFG {{
    enum "+ modPfolder.Text + @"_IO_Command
	{{
		GET			= 0,
		DELETE			= 1,
		APPEND			= 2,
		GET_ARRAY			= 3,
	}}
	enum " + modPfolder.Text + @"_IO_Flag
	{{
		GET			= 0,
	}}

	ref array<string> m_NullArray = new array<string>;
	string m_NullString = """";
	
	static string GetFile(string folderPath, string fileName, out ref array<string> contentsArray, string newContents = """", int f_IOCommand = " + modPfolder.Text + @"_IO_Command.GET, string execSide = ""Both"", bool NoChangeIfExists = 1)
	{{
		
		
		bool isDiagServer 		 = GetGame().IsMultiplayer() && GetGame().IsDedicatedServer();   
		bool isDiagMPClient		 = GetGame().IsMultiplayer() && !GetGame().IsDedicatedServer();  
		
		
		string m_TxtFileName = fileName;
		string fileContent;
		FileHandle fhandle;
		string defaultContents;
		string pth = folderPath +""/""+ m_TxtFileName;
		
		if ( FileExist(folderPath +""/""+ m_TxtFileName) && (f_IOCommand == " + modPfolder.Text + @"_IO_Command.GET || f_IOCommand == " + modPfolder.Text + @"_IO_Command.APPEND || f_IOCommand == " + modPfolder.Text + @"_IO_Command.GET_ARRAY) )
		{{
			
			
			
			fhandle	=	OpenFile(folderPath +""/""+ m_TxtFileName, FileMode.READ);		
			FGets( fhandle,  fileContent );
			CloseFile(fhandle);
			
			fhandle	=	OpenFile(folderPath +""/""+ m_TxtFileName, FileMode.READ);
			string line_content;
			while ( FGets( fhandle,  line_content ) > 0)
			{{
			
				contentsArray.Insert(line_content);
			}}
			
			
			CloseFile(fhandle);
			
			if(!NoChangeIfExists)
			{{
				if(newContents != """")
				{{
					if(newContents != fileContent)
					{{
						fileContent = newContents;
						
						if( f_IOCommand == " + modPfolder.Text + @"_IO_Command.APPEND)
						{{
							fhandle = OpenFile(folderPath +""/""+ m_TxtFileName, FileMode.APPEND);
						}}
						else 
						{{
							fhandle = OpenFile(folderPath +""/""+ m_TxtFileName, FileMode.WRITE);
						}};
						
						FPrintln(fhandle, fileContent);
						CloseFile(fhandle);
					}};
					
					Print(""[{0}] ""+execSide+"" :::  File ""+folderPath +""/""+ m_TxtFileName+"" Updated to: ""+fileContent);
				}}
			}}
			return fileContent;
		}}
		else 
		{{
			
			TStringArray parts();
			string path = folderPath;
			path.Split(""/"", parts);
			path = """";
			foreach (string part: parts)
			{{
				path += part + ""/"";
				if (part.IndexOf("":"") == part.Length() - 1)
				continue;
				
				if(f_IOCommand == " + modPfolder.Text + @"_IO_Command.GET)
				{{
					if (!FileExist(path) && !MakeDirectory(path))
					{{
						Print(""Could not make dirs from path: "" + path);
						return ""Could not make dirs from path: "" + path;
					}}
				}}
				
				if(f_IOCommand == " + modPfolder.Text + @"_IO_Command.DELETE)
				{{
					DeleteFile(pth);
					Print(""Deleted file: ""+pth);
					return ""Deleted file: ""+pth;
				}}
			}}
			
			
			FileHandle file = OpenFile(pth, FileMode.WRITE);
			if(newContents == """")
			{{
				newContents = ""You tried to create a file using {0}, but did not provide file contents. So this default content is added."";
			}}
			if (file != 0 && f_IOCommand == " + modPfolder.Text + @"_IO_Command.GET)
			{{
				FPrintln(file, newContents);
				CloseFile(file);
				
				
				
				fhandle	=	OpenFile(folderPath +""/""+ m_TxtFileName, FileMode.READ);		
				FGets( fhandle,  fileContent );
				CloseFile(fhandle);
				
				fhandle	=	OpenFile(folderPath +""/""+ m_TxtFileName, FileMode.READ);
				string line_content2;
				while ( FGets( fhandle,  line_content2 ) > 0)
				{{
				
					contentsArray.Insert(line_content2);
				}}
				
				
				Print(""[{0}] ""+execSide+"" :::  File ""+folderPath +""/""+ m_TxtFileName+"" is OK! Contents: ""+fileContent);
				CloseFile(fhandle);
				
				return fileContent;
			}}
		}}
		return ""[config] isDiagServer:""+isDiagServer+"" isDiagMPClient:""+isDiagMPClient+"" :::  File ""+folderPath +""/""+ m_TxtFileName+"" ERROR!"";
	}}
}}
", modPfolder.Text);

            string CFGfilepath = DiskPpath.Text + modPfolder.Text + "\\5_Mission\\configFile.c";
            //string text = File.ReadAllText(filepath);
            File.WriteAllText(CFGfilepath, configLibContents);

            //Create Config Lib



            //create file
            if (MissionCheckbox.Checked)
            {
                string missionServerContents = String.Format(@"modded class MissionServer
{{
    ref array<string> m_NullArray = new array<string>;
	string m_NullString = """";
    string m_StringConfig;
    int m_IntConfig;
    string m_StringFirstLineFromArray;
	void MissionServer()
	{{
		Print(""[{0}] ::: Starting Serverside"");
		ref array<string> m_CfgArray = new array<string>;
		m_StringConfig = {0}_CFG.GetFile(""$profile:\\DZR\\{0}"", ""StringConfig.txt"", m_NullArray, ""MyString"");
		m_IntConfig = {0}_CFG.GetFile(""$profile:\\DZR\\{0}"", ""IntConfig.txt"", m_NullArray, ""1"").ToInt();
		m_StringFirstLineFromArray = {0}_CFG.GetFile(""$profile:\\DZR\\{0}"", ""ArrayConfig.txt"", m_CfgArray, ""Line1\r\nLine2"");
	}}
}}", modPfolder.Text);
                ;
                string filepathMSV = DiskPpath.Text + modPfolder.Text + "\\5_Mission\\missionServer.c";
                //string text = File.ReadAllText(filepath);
                File.WriteAllText(filepathMSV, missionServerContents);
            }
            //create file

            //create file
            if (MissionCheckbox.Checked)
            {
                string missionGameplayContents = String.Format(@"modded class MissionGameplay
{{
	void MissionGameplay()
	{{
		Print(""[{0}] ::: Starting Clientside"");
	}}
}}", modPfolder.Text);
                ;
                string filepathMGP = DiskPpath.Text + modPfolder.Text + "\\5_Mission\\missionGameplay.c";
                //string text = File.ReadAllText(filepath);
                File.WriteAllText(filepathMGP, missionGameplayContents);
            }
            //create file

            //create file
            string configCppContents = String.Format(@"class CfgPatches
{{
	class {0}
	{{
		requiredAddons[] = {{""DZ_Data""}};
		units[] = {{}};
		weapons[] = {{}};
	}};
}};

class CfgMods
{{
	class {0}
	{{
		type = ""mod"";
		author = ""{1}"";
		description = ""{2}"";
		dir = ""{0}"";
		name = ""{3}"";
		//inputs = ""{0}/Data/Inputs.xml"";
		dependencies[] = {{{4}}};
		class defs
		{{
" + coreString + libString + gameString + worldString + missionString + @"
		}};
	}};
}};", modPfolder.Text, modAuthor.Text, modDescription.Text, modName.Text, requiredAddons.Text);

            string filepath = DiskPpath.Text + modPfolder.Text + "\\config.cpp";
            //string text = File.ReadAllText(filepath);
            File.WriteAllText(filepath, configCppContents);

            //common
            string CommonFilepath = DiskPpath.Text + modPfolder.Text + "\\Common\\define.c";
            string CommonContents = "#define " + modPfolder.Text.ToUpper();
            File.WriteAllText(CommonFilepath, CommonContents);
            //common


            //create file
            Process.Start("explorer.exe", DiskPpath.Text + modPfolder.Text);
            //modPfolder.Text = modPfolder.Text.ToString() + " ";
            checkAllStructure();
            processModFolderText();
            listDirs();
        }

        public void buildRequiredAddons()
        {

            string sep1 = "";
            string sep2 = "";
            string theString = "";

            theString += "\"Core\", ";
            sep1 = ", ";
            coreString = @"			class engineScriptModule
			{{
				files[] = {{""{0}/Common"",  ""{0}/1_Core""}};
			}};
";

            //theString += "\"Core\"";
            //sep1 = ", ";
            libString = @"			class gameLibScriptModule
			{{
				files[] = {{""{0}/Common"",  ""{0}/2_GameLib""}};
			}};
";

            if (GameCheckbox.Checked)
            {
                theString += "\"Game\"";
                sep1 = ", ";
                gameString = @"			class gameScriptModule
			{{
				files[] = {{""{0}/Common"",  ""{0}/3_Game""}};
			}};
";

            }
            else
            {
                gameString = "";
            }
            ;
            if (WorldCheckbox.Checked)
            {
                theString += sep1 + "\"World\"";
                sep1 = ", ";
                worldString = @"			class worldScriptModule
			{{
				files[] = {{""{0}/Common"", ""{0}/4_World""}};
			}};
";

            }
            else
            {
                worldString = "";
            };


            if (MissionCheckbox.Checked)
            {
                theString += sep1 + "\"Mission\"";
                missionString = @"			class missionScriptModule
			{{
				files[] = {{""{0}/Common"", ""{0}/5_Mission""}};
			}};
";
            }
            else
            {
                missionString = "";
            };


            requiredAddons.Text = theString;
        }

        public void createPfolder(string path, bool Checked)
        {

            if (Checked)
            {
                Directory.CreateDirectory(path);
            };
        }

        public void createPfile(string path, bool Checked)
        {

            if (Checked)
            {
                string filepath = path;
                string Contents = @"name = """ + modName.Text.ToString() + @""";
picture = """ + modPfolder.Text.ToString() + @"/Data/icon.tga"";
logo = """ + modPfolder.Text.ToString() + @"/icon.tga"";
logoSmall = """ + modPfolder.Text.ToString() + @"/icon.tga"";
logoOver = """ + modPfolder.Text.ToString() + @"/icon.tga"";
tooltip = """ + modPfolder.Text.ToString() + @""";
overview = """ + modDescription.Text.ToString() + @""";
action = """"; //URL
author = """ + modAuthor.Text.ToString() + @""";
authorID = """";
version = """"; ";
                File.WriteAllText(filepath, Contents);
            };
        }

        private void GameCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            buildRequiredAddons();
        }

        private void WorldCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            buildRequiredAddons();
        }

        private void MissionCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            buildRequiredAddons();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://steamcommunity.com/sharedfiles/filedetails/?id=1559212036");
        }

        async void ExecuteCommand2(string command)
        {
            //await Task.Delay(5000);
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;

            ConsoleLog.AppendText("output >>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            ConsoleLog.AppendText("error >>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            ConsoleLog.AppendText("ExitCode: " + exitCode.ToString());
            process.Close();
            //process.Kill();
        }
        bool ExecuteKill(string command)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = false;
            processInfo.RedirectStandardOutput = false;

            process = Process.Start(processInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            //string output = process.StandardOutput.ReadToEnd();
            //string error = process.StandardError.ReadToEnd();

            //exitCode = process.ExitCode;

            //ConsoleLog.AppendText("output >>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            // ConsoleLog.AppendText("error >>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            // ConsoleLog.AppendText("ExitCode: " + exitCode.ToString());
            process.Close();
            return true;
            //process.Kill();
        }
        void ExecuteCommand(string command)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = false;
            processInfo.RedirectStandardOutput = false;

            process = Process.Start(processInfo);
            //process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            //string output = process.StandardOutput.ReadToEnd();
            //string error = process.StandardError.ReadToEnd();

            //exitCode = process.ExitCode;

            //ConsoleLog.AppendText("output >>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            // ConsoleLog.AppendText("error >>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            // ConsoleLog.AppendText("ExitCode: " + exitCode.ToString());
            process.Close();
            //process.Kill();
        }

        private void button8_Click(object sender, EventArgs e)
        {

            string strExeFilePath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);


            string addonsPath = DiskPpath.Text.ToString() + "@" + modPfolder.Text.ToString() + @"\Addons\";
            addonsPath.Replace("/", @"\");
            Directory.CreateDirectory(Path.GetDirectoryName(addonsPath));
            string command = "";

            //-X=""thumbs.db,*.txt,*.h,*.dep,*.cpp,*.bak,*.png,*.log,*.pew, *.hpp,source,*.tga,*.bat""
            if (Packer_AddonBuilder.Checked)
            {
                 command = @""""+ ToolsDir.Text.ToString() + @"\Bin\AddonBuilder\AddonBuilder.exe""  -prefix="+PboPrefix.Text.ToString()+" " + DiskPpath.Text.ToString() + modPfolder.Text.ToString() + " " + DiskPpath.Text.ToString() + "@" + modPfolder.Text.ToString() + @"\Addons\ -packonly -include=util\include.txt";
                
            }
            else if (Packer_PboProject.Checked)
            {
                command = strExeFilePath + @"\util\pboProject.exe +C -P +B +W +N -E=dayz " + DiskPpath.Text.ToString() + modPfolder.Text.ToString() +" -Mod=" + DiskPpath.Text.ToString() + "@" + modPfolder.Text.ToString();
            }
            else
            {
                 command = strExeFilePath + @"\util\MakePbo.exe -PBW -@=" + PboPrefix.Text.ToString() + " " + DiskPpath.Text.ToString() + modPfolder.Text.ToString() + " " + DiskPpath.Text.ToString() + "@" + modPfolder.Text.ToString() + @"\Addons\";
            }
            ConsoleLog.AppendText("\r\nExecuting: " + command);

            File.WriteAllText("util\\latest_command_pack.bat", command);
            string filename = "util\\latest_command_pack.bat";
            string parameters = $"/k \"{filename}\"";
            Process.Start("cmd", parameters);

            //ExecuteCommand2(command);
            //ConsoleLog.AppendText(strExeFilePath+ "\r\n");

            Console.WriteLine(strExeFilePath);
        }
        void StartLogWatchers()
        {
            if (!watcherStarted)
            {
                try
                {
                    //start log monitoring cln
                    var watchC = new FileSystemWatcher();
                    watchC.Path = DayZPath.Text.ToString() + "\\server_profile";

                    watchC.Filter = "*.log";
                    watchC.NotifyFilter = NotifyFilters.Attributes
                                 | NotifyFilters.CreationTime
                                 | NotifyFilters.DirectoryName
                                 | NotifyFilters.FileName
                                 | NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.Security
                                 | NotifyFilters.Size;
                    //watchC.Changed += new FileSystemEventHandler(OnChanged);
                    watchC.Created += new FileSystemEventHandler(OnChangedS);
                    watchC.EnableRaisingEvents = true;
                    //start log monitoring cln

                    //start log monitoring srv
                    var watchS = new FileSystemWatcher();
                    watchS.Path = DayZPath.Text.ToString() + "\\client_profile";
                    watchS.Filter = "*.log";
                    watchS.NotifyFilter = NotifyFilters.Attributes
                                 | NotifyFilters.CreationTime
                                 | NotifyFilters.DirectoryName
                                 | NotifyFilters.FileName
                                 | NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.Security
                                 | NotifyFilters.Size;
                    //watchS.Changed += new FileSystemEventHandler(OnChanged2);
                    watchS.Created += new FileSystemEventHandler(OnChangedC);
                    watchS.EnableRaisingEvents = true;
                    //start log monitoring srv
                    ConsoleLog.AppendText(DayZPath.Text.ToString() + " log watchers started");
                    watcherStarted = true;
                }
                catch (Exception ex)
                {
                    ConsoleLog.AppendText(DayZPath.Text.ToString() + " not found");

                }
            }
        }
        /// Functions:
        void OnChangedS(object source, FileSystemEventArgs e)
        {
            // if (SrvScriptLogPath.Text.ToString() == @"D:\tmp\file.txt")
            // {
            // do stuff
            //Console.WriteLine("Reading " + e.FullPath);

           

            StartServerWatcher(e.FullPath);
           // ReadLastLine2(e.FullPath, Encoding.UTF8, "\n");

            // }
        }

        async void StartServerWatcher(string fullPath)
        {
            while (true)
            {
                ReadLastLine2(fullPath, Encoding.UTF8, "\n");
                //Console.WriteLine("Reading " + fullPath + " and waiting 5");
                //await Task.Delay(10);
            }
        }


        /// Functions:
        void OnChangedC(object source, FileSystemEventArgs e)
        {
            // if (SrvScriptLogPath.Text.ToString() == @"D:\tmp\file.txt")
            // {
            // do stuff
            //Console.WriteLine("Reading " + e.FullPath);


            StartClientWatcher(e.FullPath);
           // ReadLastLine2(e.FullPath, Encoding.UTF8, "\n");
           
            // }
        }


        async void StartClientWatcher(string fullPath)
        {
            while (true)
            {
                ReadLastLine1(fullPath, Encoding.UTF8, "\n");
                //Console.WriteLine("Reading " + fullPath + " and waiting 5");
                //await Task.Delay(10);
            }
        }


        private async void ReadLastLine1(string path, Encoding encoding, string newline)
        {

            using (FileStream logFileStream = new FileStream(@path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (StreamReader logFileReader = new StreamReader(logFileStream))
                {
                    string line;
                    while (!logFileReader.EndOfStream)
                    {
                        line = logFileReader.ReadLine();
                        if (logFileReader.Peek() == -1)
                        {
                            //ConsoleLog.Invoke( () => ConsoleLog.AppendText("\r\n" + line));
                            if (thePreviousLastLine1 != line)
                            {
                                thePreviousLastLine1 = line;
                                Action action = () => ConsoleLog.AppendText("\r\n⬛CLIENT⬛ ::: " + line);
                                if (ConsoleLog.InvokeRequired)
                                {
                                    ConsoleLog.Invoke(action);
                                }
                                else
                                {
                                    action();
                                }
                            }

                        }
                    }
                }
            }

        }

        private async void ReadLastLine2(string path, Encoding encoding, string newline)
        {

            using (FileStream logFileStream = new FileStream(@path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (StreamReader logFileReader = new StreamReader(logFileStream))
                {
                    string line;
                    while (!logFileReader.EndOfStream)
                    {
                        line = logFileReader.ReadLine();
                        if (logFileReader.Peek() == -1)
                        {
                            //ConsoleLog.Invoke( () => ConsoleLog.AppendText("\r\n" + line));
                            if (thePreviousLastLine2 != line)
                            {
                                thePreviousLastLine2 = line;
                                Action action = () => ConsoleLog.AppendText("\r\n⬛⬛SERVER⬛ ::: " + line);
                                if (ConsoleLog.InvokeRequired)
                                {
                                    ConsoleLog.Invoke(action);
                                }
                                else
                                {
                                    action();
                                }
                            }

                        }
                    }
                }
            }

        }


        private async void ReadTail2(string filename)
        {
            using (FileStream fs = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                // Seek 1024 bytes from the end of the file
                fs.Seek(-1024, SeekOrigin.End);
                // read 1024 bytes
                byte[] bytes = new byte[1024];
                fs.Read(bytes, 0, 1024);
                // Convert bytes to string
                string s = Encoding.Default.GetString(bytes);
                // or string s = Encoding.UTF8.GetString(bytes);
                // and output to console
                ConsoleLog.AppendText("|||||||||| " + s);
                fs.Close();

            }
        }

        private async void button9_Click(object sender, EventArgs e)
        {


            string dayFolderPath = DayZPath.Text.ToString();
            Console.WriteLine("\r\ndayFolderPath " + dayFolderPath);
            string command = "";
            Properties.Settings.Default.Save();
            if (killProcesses.Checked)
            {
                command += @"@echo off
taskkill /f /im DayZDiag_x64.exe";
                command += @"
taskkill /f /im MakePbo.exe";

                //ExecuteCommand(command);
                if (packBeforeStart.Checked)
                {
                    string strExeFilePath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);

                    command += "\r\ntimeout 1";
                    string addonsPath = DiskPpath.Text.ToString() + "@" + modPfolder.Text.ToString() + @"\Addons\";
                    addonsPath.Replace("/", @"\");
                    Directory.CreateDirectory(Path.GetDirectoryName(addonsPath));
                    //await Task.Delay(2500);
                    /*-PsBW
- X = thumbs.db,*.txt,*.h,*.dep,*.cpp,*.bak,*.png,*.log,*.pew, *.hpp,source,*.tga,*.bat*/

                    //-X=""thumbs.db,*.txt,*.h,*.dep,*.cpp,*.bak,*.png,*.log,*.pew, *.hpp,source,*.tga,*.bat""
                    command += "\r\n" + strExeFilePath + @"\util\MakePbo.exe -PBW -@=" + PboPrefix.Text.ToString() + " " + DiskPpath.Text.ToString() + modPfolder.Text.ToString() + " " + DiskPpath.Text.ToString() + "@" + modPfolder.Text.ToString() + @"\Addons\";
                    ConsoleLog.AppendText("\r\nExecuting: " + command);
                    //command += "\r\ntimeout 5";
                    ExecuteCommand(command);
                }

            };
            //


            if (isServerMod.Checked)
            {
                command += "\r\nstart \"\" \"" + dayFolderPath + "\\DayZDiag_x64.exe\" -config=serverDZ.cfg -server -port=2302 \"-profiles=" + dayFolderPath + "\\server_profile\" "+ LaunchParams.Text.ToString() + " -dologs -adminlog -noPause -filePatching -freezecheck \"-mod=" + prefixMods.Text.ToString() + "\" \"-servermod=" + serverModPrefix.Text.ToString() + DiskPpath.Text.ToString().Replace(@"\", "/") + "@" + modPfolder.Text.ToString() + ";\"";
            }
            else
            {

                if (useServerMods.Checked)
                {
                    string newPath = dayFolderPath.Replace(@"\", @"\\");
                    newPath = @"C:\SteamLibrary\steamapps\common\Day Z";
                    command += "\r\nstart \"\" \"" + newPath + "\\DayZDiag_x64.exe\" -config=serverDZ.cfg -server -port=2302 \"-profiles=" + dayFolderPath + "\\server_profile\" " + LaunchParams.Text.ToString() + " -dologs -adminlog -noPause -filePatching -freezecheck \"-mod=" + prefixMods.Text.ToString() + "" + DiskPpath.Text.ToString().Replace(@"\", "/") + "@" + modPfolder.Text.ToString() + ";\"" + " \"-servermod=" + serverModPrefix.Text.ToString() + ";\"";
                }
                else
                {
                    command += "\r\nstart \"\" \"" + dayFolderPath + "\\DayZDiag_x64.exe\" -config=serverDZ.cfg -server -port=2302 \"-profiles=" + dayFolderPath + "\\server_profile\" " + LaunchParams.Text.ToString() + " -dologs -adminlog -noPause -filePatching -freezecheck \"-mod=" + prefixMods.Text.ToString() + "" + DiskPpath.Text.ToString().Replace(@"\", "/") + "@" + modPfolder.Text.ToString() + ";\"";
                }



            };

            //ExecuteCommand(command);


            if (startBothCheckbox.Checked)
            {
                command += "\r\ntimeout 18";
                //command += "\r\npause\r\n";
                if (isServerMod.Checked)
                {
                    command += "\r\nstart \"\" \"" + dayFolderPath + "\\DayZDiag_x64.exe\" \"-profiles=" + dayFolderPath + "\\client_profile\" " + LaunchParams.ToString() + " -dologs -adminlog -noPause -filePatching -name=" + playerName.Text.ToString() + " -connect=127.0.0.1 -port=2302 \"-mod=" + prefixMods.Text.ToString() + "\"";

                }
                else
                {
                    command += "\r\nstart \"\" \"" + dayFolderPath + "\\DayZDiag_x64.exe\" \"-profiles=" + dayFolderPath + "\\client_profile\" " + LaunchParams.ToString() + " -dologs -adminlog -noPause -filePatching -name=" + playerName.Text.ToString() + " -connect=127.0.0.1 -port=2302 \"-mod=" + prefixMods.Text.ToString() + "" + DiskPpath.Text.ToString().Replace(@"\", "/") + "@" + modPfolder.Text.ToString() + ";\"";
                }


            }
            command += "\r\ntimeout 2 >nul\r\nexit";
            ConsoleLog.AppendText("\r\nExecuting:\r\n" + command);
            string batPath = dayFolderPath + "\\" + modPfolder.Text.ToString() + ".bat";

            // Create a file to write to.
            using (StreamWriter sw = File.CreateText(batPath))
            {
                sw.WriteLine(command);
            }
            string execString = "\r\nstart \"\" " + batPath;

            File.WriteAllText("util\\latest_command_server.bat", command);
            string filename = "util\\latest_command_server.bat";
            string parameters = $"/k \"{filename}\"";
            Process.Start("cmd", parameters);

            //ExecuteCommand(execString);

        }

        private void DiskPpath_TextChanged(object sender, EventArgs e)
        {
            //DiskPpath.Text = DiskPpath.Text.ToString()
            listDirs();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            string command = "\r\ntaskkill /f /im \"MakePbo.exe\"";
            ExecuteCommand(command);
        }

        private void openpFolder_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", DiskPpath.Text + modPfolder.Text);
        }

        private void openPackFolder_Click(object sender, EventArgs e)
        {
            string addonsPath = DiskPpath.Text.ToString() + "@" + modPfolder.Text.ToString() + @"\Addons\";
            addonsPath.Replace("/", @"\");
            Directory.CreateDirectory(Path.GetDirectoryName(addonsPath));
            Process.Start("explorer.exe", DiskPpath.Text + "@" + modPfolder.Text+@"\Addons");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string command = "\r\ntaskkill /f /im \"DayZDiag_x64.exe\"";
            Properties.Settings.Default.Save();
            ExecuteCommand(command);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", DayZPath.Text.ToString() + "\\client_profile");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", DayZPath.Text.ToString() + "\\server_profile");
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            // File.WriteAllText("scrathpad.txt", textBox1.Text);
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            File.WriteAllText("scratchpad.txt", textBox1.Text);
            //Properties.Settings.Default.Save();
        }

        private void isServerMod_CheckedChanged(object sender, EventArgs e)
        {
            if (isServerMod.Checked)
            {
                isServerMod.BackColor = Color.Orange;
            }
            else
            {
                isServerMod.BackColor = SystemColors.Window;
            }
        }

        private void modPfolder_KeyUp(object sender, KeyEventArgs e)
        {
            processModFolderText();
        }

        private void processModFolderText()
        {

            modPfolder.Text = modPfolder.Text.Trim();
            string fullPath = DiskPpath.Text.ToString() + modPfolder.Text.ToString();
            Console.WriteLine("[modPfolder.Text]: " + modPfolder.Text.ToString());
            if (Directory.Exists(fullPath))
            {
                listDirs();
            }

            if (File.Exists(@fullPath + @"\config.cpp"))
            {
                Console.WriteLine("config.cpp exists");
                modPfolder.BackColor = Color.LimeGreen;
                checkAllStructure();
                createStructure.Enabled = false;
                openpFolder.Enabled = true;
                openPackFolder.Enabled = true;
                CheckBoxPanel.Enabled = false;
                packPbo.Enabled = true;
            }
            else
            {
                modPfolder.BackColor = SystemColors.Window;
                checkAllStructure();

                if (modPfolder.Text.Trim() != "")
                {
                    createStructure.Enabled = true;
                    CheckBoxPanel.Enabled = true;
                    openpFolder.Enabled = false;
                    openPackFolder.Enabled = false;
                    packPbo.Enabled = false;
                }
                else
                {
                    createStructure.Enabled = false;
                    CheckBoxPanel.Enabled = false;
                    openpFolder.Enabled = true;
                    openPackFolder.Enabled = true;
                    packPbo.Enabled = true;
                }
            }
            checkAllStructure();

            Console.WriteLine("[FULL PATH]: " + fullPath + "\\config.cpp :" + File.Exists(fullPath + "\\config.cpp").ToString());
        }

        private void button8_Click_1(object sender, EventArgs e)
        {

            ConsoleLog.AppendText(CustomFolder.Text.ToString());
            string fullPath = DiskPpath.Text.ToString() + modPfolder.Text.ToString() + "\\" + CustomFolder.Text.ToString().Replace("/", @"\");

            //fullPath.Replace("/", @"\");

            ConsoleLog.AppendText(fullPath);
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
            }
            if (!File.Exists(fullPath))
            {
                File.WriteAllText(fullPath, "");
            }
            listFiles();
            Process.Start("explorer.exe", fullPath);
        }

        public void GetSubDirectories()

        {

            string root = @"C:\Temp";

            // Get all subdirectories

            string[] subdirectoryEntries = Directory.GetDirectories(root);

            // Loop through them to see if they have any other subdirectories

            foreach (string subdirectory in subdirectoryEntries)

                LoadSubDirs(subdirectory);

        }

        private void LoadSubDirs(string dir)

        {

            Console.WriteLine(dir);

            string[] subdirectoryEntries = Directory.GetDirectories(dir);

            foreach (string subdirectory in subdirectoryEntries)

            {

                LoadSubDirs(subdirectory);

            }

        }

        void listDirs()
        {
            pDirectories.Items.Clear();

            String[] dirs = System.IO.Directory.GetDirectories(DiskPpath.Text.ToString());
            int i;
            for (i = 0; i < dirs.Length; i++)
            {
                Console.WriteLine(dirs[i].ToString());
                if (dirs[i].ToString().ToLower() != @"p:\core" && dirs[i].ToString().ToLower() != @"p:\bin" && dirs[i].ToString().ToLower() != @"p:\gui" && dirs[i].ToString().ToLower() != @"p:\dz" && dirs[i].ToString().ToLower() != @"p:\system" && dirs[i].ToString().ToLower() != @"p:\scripts" && !dirs[i].ToString().Contains('@'))
                {
                    pDirectories.Items.Add(dirs[i].Replace(DiskPpath.Text.ToString(), ""));
                }
            }

        }





        void listFiles(bool open = false)
        {
            filesInDir.Items.Clear();
            int i;
            //TODO: Search multiple fyle extensions
            String[] files = System.IO.Directory.GetFiles(DiskPpath.Text.ToString() + "\\" + modPfolder.Text.ToString(), "*.*", SearchOption.AllDirectories);
            string fullPath;
            for (i = 0; i < files.Length; i++)
            {
                if (allowedExtension(files[i]))
                {

                    if (open)
                    {

                        //fullPath = DiskPpath.Text.ToString() + modPfolder.Text.ToString() + @"\" + files[i];
                        fullPath = files[i].Replace(@"\\", @"\");
                        ConsoleLog.AppendText("\r\n" + fullPath);
                        Process.Start("explorer.exe", fullPath);
                        fullPath = "";
                    }
                    filesInDir.Items.Add(files[i].Replace(DiskPpath.Text.ToString() + "\\" + modPfolder.Text.ToString() + @"\", ""));
                }
            }
        }
        bool allowedExtension(string filename)
        {
            if (FilterCheckbox.Checked)
            {
                if (filename.EndsWith(".c", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                };
                if (filename.EndsWith(".cpp", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                };
                if (filename.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                };
                if (filename.EndsWith(".hpp", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                };
                /*if (filename.EndsWith(".rvmat", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                };*/
                if (filename.EndsWith(".txt", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                };
                if (filename.EndsWith(".json", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                };
            }
            else
            {
                return true;
            }
            return false;
        }
        private void pDirectories_SelectedIndexChanged(object sender, EventArgs e)
        {
            modPfolder.Text = pDirectories.SelectedItem.ToString();
            checkAllStructure();
            listFiles();
        }

        private void filesInDir_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void filesInDir_DoubleClick(object sender, EventArgs e)
        {
            ConsoleLog.AppendText("\r\nOPEN FILE: " + DiskPpath.Text.ToString() + modPfolder.Text.ToString() + @"\" + filesInDir.Text.ToString());
            string fullPath = DiskPpath.Text.ToString() + modPfolder.Text.ToString() + @"\" + filesInDir.Text.ToString();
            Process.Start("explorer.exe", fullPath);
        }



        private void OpenLastFilyByName(string namePart, string dir)
        {

            DirectoryInfo info = new DirectoryInfo(DayZPath.Text.ToString() + "\\" + dir);
            FileInfo[] files = info.GetFiles().OrderByDescending(p => p.LastWriteTime).ToArray();
            foreach (FileInfo file in files)
            {
                if (file.Name.Contains(namePart))
                {
                    Process.Start("explorer.exe", file.DirectoryName + "\\" + file.Name);
                    return;
                }
                // DO Something...
                //Process.Start("explorer.exe", DayZPath.Text.ToString() + "\\server_profile");
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (startBothCheckbox.Checked)
            {
                startClientBut.Enabled = false;
            }
            else
            {
                startClientBut.Enabled = true;
            }


        }

        private void startClientBut_Click(object sender, EventArgs e)
        {
            string dayFolderPath = DayZPath.Text.ToString();
            string command = "";
            Properties.Settings.Default.Save();

            command += "@echo off\r\n";
            //command += "\r\npause\r\n";
            if (isServerMod.Checked)
            {
                command += "\r\nstart \"\" \"" + dayFolderPath + "\\DayZDiag_x64.exe\" \"-profiles=" + dayFolderPath + "\\client_profile\" " + LaunchParams.Text.ToString() + " -dologs -adminlog -noPause -filePatching -name=" + playerName.Text.ToString() + " -connect=127.0.0.1 -port=2302 \"-mod=" + prefixMods.Text.ToString() + "\"";

            }
            else
            {
                command += "\r\nstart \"\" \"" + dayFolderPath + "\\DayZDiag_x64.exe\" \"-profiles=" + dayFolderPath + "\\client_profile\" " + LaunchParams.Text.ToString() + " -dologs -adminlog -noPause -filePatching -name=" + playerName.Text.ToString() + " -connect=127.0.0.1 -port=2302 \"-mod=" + prefixMods.Text.ToString() + "" + DiskPpath.Text.ToString().Replace(@"\", "/") + "@" + modPfolder.Text.ToString() + ";\"";
            }



            command += "\r\ntimeout 2 >nul\r\nexit";
            ConsoleLog.AppendText("\r\nExecuting:\r\n" + command);
            string batPath = dayFolderPath + "\\" + modPfolder.Text.ToString() + ".bat";

            // Create a file to write to.
            using (StreamWriter sw = File.CreateText(batPath))
            {
                sw.WriteLine(command);
            }
            string execString = "start \"\" " + batPath;
            //ExecuteCommand(execString);
            File.WriteAllText("util\\latest_command_client.bat", command);
            string filename = "util\\latest_command_client.bat";
            string parameters = $"/k \"{filename}\"";
            Process.Start("cmd", parameters);

        }

        private void button12_Click(object sender, EventArgs e)
        {
            OpenLastFilyByName("script_", "server_profile");
        }

        private void SRVrpt_Click(object sender, EventArgs e)
        {
            OpenLastFilyByName(".RPT", "server_profile");
        }

        private void SRVcrash_Click(object sender, EventArgs e)
        {
            OpenLastFilyByName("crash", "server_profile");
        }

        private void CNTscript_Click(object sender, EventArgs e)
        {
            OpenLastFilyByName("script_", "client_profile");
        }

        private void CNTrpt_Click(object sender, EventArgs e)
        {
            OpenLastFilyByName(".RPT", "client_profile");
        }

        private void CNTcrash_Click(object sender, EventArgs e)
        {
            OpenLastFilyByName("crash", "client_profile");
        }

        void deleteFilesByExt(string ext, string path)
        {
            string fullPath = DayZPath.Text.ToString() + "\\" + path;
            DirectoryInfo di = new DirectoryInfo(fullPath);
            FileInfo[] files = di.GetFiles("*." + ext)
                                 .Where(p => p.Extension == "." + ext).ToArray();
            foreach (FileInfo file in files)
                try
                {
                    file.Attributes = FileAttributes.Normal;
                    File.Delete(file.FullName);
                }
                catch { }
        }

        private void clearLogs_Click(object sender, EventArgs e)
        {
            deleteFilesByExt("RPT", "server_profile");
            deleteFilesByExt("mdmp", "server_profile");
            deleteFilesByExt("log", "server_profile");
            deleteFilesByExt("ADM", "server_profile");


        }

        private void button12_Click_1(object sender, EventArgs e)
        {
            deleteFilesByExt("RPT", "client_profile");
            deleteFilesByExt("mdmp", "client_profile");
            deleteFilesByExt("log", "client_profile");
            deleteFilesByExt("ADM", "client_profile");
        }

        private void FilterCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            listFiles();
        }

        private void openAllFiles_Click(object sender, EventArgs e)
        {
            listFiles(true);



        }

        private void pDirectories_MouseEnter(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure to delete "+ DiskPpath.Text.ToString() + modPfolder.Text.ToString(), "Deleting folder", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                /// do something here        

                ConsoleLog.AppendText("\r\nDELETE DIR: " + DiskPpath.Text.ToString() + modPfolder.Text.ToString());
                string fullPath = DiskPpath.Text.ToString() + modPfolder.Text.ToString();// + @"\" + pDirectories.Text.ToString();
                                                                                         //Process.Start("explorer.exe", fullPath);
                                                                                         //Console.WriteLine("@RD /S /Q " + fullPath);
                ExecuteCommand("@RD /S /Q " + fullPath);
                listDirs();
                listFiles();
            }

        }

        private void button14_Click(object sender, EventArgs e)
        {
            string fullPath = DiskPpath.Text.ToString() + modPfolder.Text.ToString() + @"\" + filesInDir.Text.ToString();
            DialogResult dialogResult = MessageBox.Show("Are you sure to delete " + fullPath, "Deleting file", MessageBoxButtons.YesNo);
            
            if (dialogResult == DialogResult.Yes)
            {
                ConsoleLog.AppendText("\r\nDELETE FILE: " + fullPath);
                
                //Process.Start("explorer.exe", fullPath);
                try
                {
                    //file.Attributes = FileAttributes.Normal;
                    // Console.WriteLine(fullPath);
                    File.Delete(fullPath);
                    listFiles();
                }
                catch { }
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            listDirs();
            listFiles();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            string execString = "TortoiseGitProc /command:sync /path:" + DiskPpath.Text + modPfolder.Text;
            ExecuteCommand(execString);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            string execString = "TortoiseGitProc /command:commit /path:" + DiskPpath.Text + modPfolder.Text;
            ExecuteCommand(execString);
        }

        private void pDirectories_DoubleClick(object sender, EventArgs e)
        {
            if (isServerMod.Checked)
            {
                button9_Click(sender, e);
            }
            else
            {
                startClientBut_Click(sender, e);
            }
        }

        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {

        }

        private void button18_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", DayZPath.Text.ToString() + "\\client_profile\\DZR");
        }

        private void button19_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", DayZPath.Text.ToString() + "\\server_profile\\DZR");
        }

        private void OpenWorkshop_Click(object sender, EventArgs e)
        {
            string addonsPath = DayZPath.Text.ToString() + @"\!Workshop\";
            addonsPath.Replace("/", @"\");
            //Directory.CreateDirectory(Path.GetDirectoryName(addonsPath));
            Process.Start("explorer.exe", DayZPath.Text.ToString() + @"\!Workshop\");
        }

        private void OpenProfiles_Click(object sender, EventArgs e)
        {
            string addonsPath = DayZPath.Text.ToString() + @"\mpmissions\";
            addonsPath.Replace("/", @"\");
           // Directory.CreateDirectory(Path.GetDirectoryName(addonsPath));
            Process.Start("explorer.exe", DayZPath.Text.ToString() + @"\mpmissions\");
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", DayZPath.Text.ToString() + @"\mpmissions\dayzOffline.chernarusplus\init.c");
        }

        private void button21_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", DayZPath.Text.ToString() + @"\mpmissions\dayzOffline.chernarusplus\db\globals.xml");
        }

        private void packBeforeStart_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void packBeforeStart_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void killProcesses_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void useServerMods_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void WorkshopMods_AfterCheck(object sender, TreeViewEventArgs e)
        {
            
            //CheckedNames(WorkshopMods.Nodes);
            


        }

        private void PrintRecursive(TreeNode treeNode)
        {
            // Print the node.
            //System.Diagnostics.Debug.WriteLine(treeNode.Text);
            //MessageBox.Show(selected.ToString());
            //MessageBox.Show(WorkshopMods.Nodes[0][selected].Text);
            // Print each node recursively.
           
            string aResult = "";
            foreach (TreeNode tn in treeNode.Nodes)
            {
                if (tn.Checked)
                {
                    //PrintRecursive(tn);
                    //Console.WriteLine(tn);
                    aResult += "!Workshop/@"+tn.Text+";";
                }
            }
            Console.WriteLine(aResult);
            prefixMods.Text = aResult;
            Properties.Settings.Default.Save();

            WorkshopMods.Nodes.Clear();
            WorkshopMods.Nodes.Add(TraverseDirectory());
            WorkshopMods.ExpandAll();
           // WorkshopMods.Nodes[selected+1].EnsureVisible();
        }

        private string GetModline(TreeNode treeNode)
        {
            // Print the node.
            //System.Diagnostics.Debug.WriteLine(treeNode.Text);
            //MessageBox.Show(selected.ToString());
            //MessageBox.Show(WorkshopMods.Nodes[0][selected].Text);
            // Print each node recursively.

            string aResult = "";
            foreach (TreeNode tn in treeNode.Nodes)
            {
                if (tn.Checked)
                {
                    //PrintRecursive(tn);
                    //Console.WriteLine(tn);
                    aResult += "!Workshop/@" + tn.Text + ";";
                }
            }
            aResult += "\r\n" + serverModPrefix.Text;
            Console.WriteLine(aResult);
            return aResult;
            // WorkshopMods.Nodes[selected+1].EnsureVisible();
        }

        void CheckedNames(System.Windows.Forms.TreeNodeCollection theNodes)
        {
            string aResult = "";
            Console.WriteLine(theNodes);
            if (theNodes[0] != null)
            {
                foreach (System.Windows.Forms.TreeNode aNode in theNodes)
                {
                    Console.WriteLine(aNode.Tag);
                    if (aNode.Checked)
                    {
                        aResult += aNode.Text;
                        //Console.WriteLine(aNode.Tag);
                        //PrintRecursive(n);
                    }
                    //aResult.AddRange(CheckedNames(aNode.Nodes));
                }
            }
            prefixMods.Text = aResult;
            Console.WriteLine(aResult);
            //return aResult;
        }

        private void RefreshMods_Click(object sender, EventArgs e)
        {
            PrintRecursive(WorkshopMods.Nodes[0]);
        }

        private void button22_Click(object sender, EventArgs e)
        {
            PrintRecursive(WorkshopMods.Nodes[0]);
            string filename = "presets/" + presetName.Text + ".txt";
            string presetContent = prefixMods.Text+"\r\n" + serverModPrefix.Text; //GetModline(WorkshopMods.Nodes[0]);
            File.WriteAllText(filename, presetContent);
            presetsList.Items.Clear();
            listPresets();
            presetName.Text = "";

        }

        void listPresets()
        {
            filesInDir.Items.Clear();
            int i;
            //TODO: Search multiple fyle extensions
            String[] files = System.IO.Directory.GetFiles("presets/", "*.txt", SearchOption.AllDirectories);
            string fullPath;
            for (i = 0; i < files.Length; i++)
            {
                if (allowedExtension(files[i]))
                {

      

                        //fullPath = DiskPpath.Text.ToString() + modPfolder.Text.ToString() + @"\" + files[i];

                    
                        fullPath = files[i].Replace(@"\\", @"\");
                    presetsList.Items.Add(Path.GetFileNameWithoutExtension(fullPath));
                    ConsoleLog.AppendText("\r\n" + Path.GetFileNameWithoutExtension(fullPath));
                       // Process.Start("explorer.exe", fullPath);
                        fullPath = "";
                    
                   // filesInDir.Items.Add(files[i].Replace(DiskPpath.Text.ToString() + "\\" + modPfolder.Text.ToString() + @"\", ""));
                }
                presetsList.SelectedIndex = 0;
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            ProcessPreset();
        }

        void ProcessPreset(int mode = 0)
        {
            //string alltext = File.ReadAllText("scratchpad.txt");
            string FileName = "presets/" + presetsList.SelectedItem.ToString() + ".txt";
            StreamReader sr = new StreamReader(FileName);
            string line = "";
            List<string> lines = new List<string>();
            while ((line = sr.ReadLine()) != null)
            {
                lines.Add(line);
            };
            sr.Close();
            prefixMods.Text = lines[0];
            if (lines.Count() == 1)
            {
                serverModPrefix.Text = "";
            }
            else
            {
                serverModPrefix.Text = lines[1];
            }
            string theLine = lines[0].Replace("!Workshop/@", "");

            string[] modsFull = lines[0].Split(';');


            List<string> modsNames = new List<string>();
            foreach (var item in modsFull)
            {
                // List<string> temparr = new List<string>();
                string[] temparr = item.Split('@');
                //modsNames.Add(temparr[1]);
                if (temparr.Count() > 1)
                {
                    modsNames.Add(temparr[1]);
                    //ConsoleLog.AppendText("\r\n" + temparr[1]);
                }



            };

            foreach (TreeNode tn in WorkshopMods.Nodes[0].Nodes)
            {


                //ConsoleLog.AppendText("\r\n-------");
                //ConsoleLog.AppendText("\r\ntn.Text: " + tn.Text);
                //ConsoleLog.AppendText("\r\nmodName:" + modName);
                if (mode == 0)
                {
                    if (modsNames.Contains(tn.Text))
                    {
                        tn.Checked = true;
                        // ConsoleLog.AppendText("\r\n" + modName);

                    }
                    else
                    {
                        tn.Checked = false;
                    }
                }

                if (mode == 1)
                {
                    if (modsNames.Contains(tn.Text))
                    {
                        tn.Checked = true;
                        // ConsoleLog.AppendText("\r\n" + modName);

                    }
                }

                if (mode == 2)
                {
                    if (modsNames.Contains(tn.Text))
                    {
                        tn.Checked = false;
                        // ConsoleLog.AppendText("\r\n" + modName);

                    }
                }

            }
            PrintRecursive(WorkshopMods.Nodes[0]);
            ConsoleLog.AppendText("\r\nPreset " + FileName + " Loaded");
        }

        private void button24_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Delete preset " + presetsList.Items[0] + "?", "DELETION CONFIRMATION" ,MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                string FileName = "presets/" + presetsList.Items[0] + ".txt";
                File.Delete(FileName);
                presetsList.Items.Clear();
                listPresets();
            }

        }

        private void button25_Click(object sender, EventArgs e)
        {
            ProcessPreset(1);
        }

        private void button26_Click(object sender, EventArgs e)
        {
            ProcessPreset(2);
        }

        private void button27_Click(object sender, EventArgs e)
        {
            //string fullPath = DiskPpath.Text.ToString() + modPfolder.Text.ToString() + @"\" + filesInDir.Text.ToString();
            DialogResult dialogResult = MessageBox.Show("Are you sure to wipe C:\\SteamLibrary\\steamapps\\common\\DayZ\\mpmissions\\dayzOffline.chernarusplus", "Deleting mission data", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                ConsoleLog.AppendText(@"\r\nDELETE DIR: C:\SteamLibrary\steamapps\common\DayZ\mpmissions\dayzOffline.chernarusplus\storage_1");
                ConsoleLog.AppendText(@"\r\nDELETE DIR: C:\SteamLibrary\steamapps\common\DayZ\mpmissions\dayzOffline.chernarusplus\storage_0");
                string fullPath = @"C:\SteamLibrary\steamapps\common\DayZ\mpmissions\dayzOffline.chernarusplus\storage_1";// + @"\" + pDirectories.Text.ToString();
                string fullPath0 = @"C:\SteamLibrary\steamapps\common\DayZ\mpmissions\dayzOffline.chernarusplus\storage_0";// + @"\" + pDirectories.Text.ToString();
                                                                                                                           //Process.Start("explorer.exe", fullPath);
                                                                                                                           //Console.WriteLine("@RD /S /Q " + fullPath);
                ExecuteCommand("@RD /S /Q " + fullPath);
                ExecuteCommand("@RD /S /Q " + fullPath0);
            }
        }

        private void button28_Click(object sender, EventArgs e)
        {

                if (BrowseDayZ.ShowDialog() == DialogResult.OK)
                {
                    ToolsDir.Text = BrowseDayZ.SelectedPath;

                }
            
        }

        private void StartWorkbench_Click(object sender, EventArgs e)
        {
            if (StartWorkbench.Text == "START WORKBENCH")
            {

                StartWorkbench.Text = "STOP WORKBENCH";
                StartWorkbench.BackColor = Color.Maroon;

                string dayFolderPath = ToolsDir.Text.ToString();
                string command = "";
                Properties.Settings.Default.Save();
                // start /D "D:\Games\SteamLibrary\steamapps\common\DayZ Tools\Bin\Workbench" workbenchApp.exe - mod = P:\@GGmod
                command += "@echo off\r\n";
                //command += "\r\npause\r\n";
                command += "\r\nstart /D \"" + dayFolderPath + "\"\\Bin\\Workbench\\workbenchApp.exe \"-mod=" + prefixMods.Text.ToString() + "" + DiskPpath.Text.ToString().Replace(@"\", "/") + "@" + modPfolder.Text.ToString() + ";\"";

                command += "\r\ntimeout 2 >nul\r\nexit";
                ConsoleLog.AppendText("\r\nExecuting:\r\n" + command);
                string batPath = dayFolderPath + "\\" + modPfolder.Text.ToString() + ".bat";

                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(batPath))
                {
                    sw.WriteLine(command);
                }
                //string execString = "start \"\" " + batPath;
                //ExecuteCommand(execString);
                File.WriteAllText("util\\latest_command_workbench.bat", command);
                string filename = "util\\latest_command_workbench.bat";
                string parameters = $"/k \"{filename}\"";
                Process.Start("cmd", parameters);
            }
            else
            {
                StartWorkbench.Text = "START WORKBENCH";
                StartWorkbench.BackColor = Color.FromArgb(64, 64, 64);
                string command = "\r\ntaskkill /f /im \"workbenchApp.exe\"";
                Properties.Settings.Default.Save();
                ExecuteCommand(command);
            }
        }

        private void ToolsDir_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.ToolsDir = ToolsDir.Text.ToString();
            Properties.Settings.Default.Save();
            
            if (Directory.Exists(ToolsDir.Text.ToString()))
            {
                ToolsDir.BackColor = Color.Lime;
                Packer_AddonBuilder.Enabled = true;
                //StartLogWatchers();
            }
            else
            {
                ConsoleLog.AppendText(ToolsDir.Text.ToString() + ": path not found");
                ToolsDir.BackColor = Color.Red;
                Packer_AddonBuilder.Enabled = false;
            }
            Properties.Settings.Default.Reload();
        }

        private void button29_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", DayZPath.Text.ToString() + @"\serverDZ.cfg");
        }

        private void button30_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", "util");
            
        }

        private void prefixMods_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.prefixMods = prefixMods.Text.ToString();
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();
        }
        
        private void Packer_MakePbo_CheckedChanged(object sender, EventArgs e)
        {
            
            Properties.Settings.Default.MakePbo = Packer_MakePbo.Checked;
            savePacked();
        }

        private void Packer_PboProject_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.PboProject = Packer_PboProject.Checked;
            savePacked();
        }

        private void Packer_AddonBuilder_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.AddonBuilder = Packer_AddonBuilder.Checked;
            savePacked();
        }

        void savePacked()
        {
            
            
            
            Properties.Settings.Default.Save();
            //Properties.Settings.Default.Reload();
            /*
            ConsoleLog.AppendText("\r\nSaved Packer_AddonBuilder:\r\n" + Packer_AddonBuilder.Checked);
            ConsoleLog.AppendText("\r\nSaved Packer_PboProject:\r\n" + Packer_PboProject.Checked);
            ConsoleLog.AppendText("\r\nSaved Packer_AddonBuilder:\r\n" + Packer_AddonBuilder.Checked);

            ConsoleLog.AppendText("\r\n---------\r\nPacker_AddonBuilder:\r\n" + Properties.Settings.Default.MakePbo);
            ConsoleLog.AppendText("\r\nPacker_PboProject:\r\n" + Properties.Settings.Default.PboProject);
            ConsoleLog.AppendText("\r\nPacker_AddonBuilder:\r\n" + Properties.Settings.Default.AddonBuilder);
            */
        }

        private void Packer_MakePbo_Click(object sender, EventArgs e)
        {
            
        }

        private void groupBox5_TabIndexChanged(object sender, EventArgs e)
        {

        }

        private void CreateServerBat_Click(object sender, EventArgs e)
        {

            string dayFolderPath = DayZPath.Text.ToString();
            Console.WriteLine("\r\ndayFolderPath " + dayFolderPath);
            string command = @"
:a
cls
@echo off
taskkill /f /im DayZ_x64.exe
timeout 2 > nul
taskkill /f /im DayZServer_x64.exe
pause

start """" ""DayZServer_x64.exe"" -config=serverDZ_humanity.cfg -server -port=2302 -profiles=server_profile -newErrorsAreWarnings=1 -dologs -adminlog -noPause ""-mod=";

            command += prefixMods.Text.ToString() + "" + DiskPpath.Text.ToString().Replace(@"\", "/") + "@" + modPfolder.Text.ToString() + ";\"";

            if(useServerMods.Checked)
            {
                command += " \"-servermod=" + serverModPrefix.Text.ToString() + "\"";
            }

            command += @"

pause
goto a";


            ConsoleLog.AppendText("\r\nGenerating Server Bat:\r\n" + command);
            if (ServerDir.Text == "")
            {
                File.WriteAllText("util\\DayZ_Server_" + modPfolder.Text.ToString() + ".bat", command);
                Process.Start("explorer.exe", "util");
            }
            else
            {
                File.WriteAllText( ServerDir.Text + "\\DayZ_Server_" + modPfolder.Text.ToString() + ".bat", command);
            }
            //string filename = "util\\latest_command_server.bat";
            //string parameters = $"/k \"{filename}\"";
            //Process.Start("cmd", parameters);
        }

        private void button31_Click(object sender, EventArgs e)
        {

            if (BrowseDayZ.ShowDialog() == DialogResult.OK)
            {
                ServerDir.Text = BrowseDayZ.SelectedPath;

            }
        }

        private void ServerDir_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.ServerDir = ServerDir.Text.ToString();
            Properties.Settings.Default.Save();

            if (Directory.Exists(ServerDir.Text.ToString()))
            {
                ServerDir.BackColor = Color.Lime;
                Packer_AddonBuilder.Enabled = true;
                //StartLogWatchers();
            }
            else
            {
                ConsoleLog.AppendText(ToolsDir.Text.ToString() + ": path not found");
                ServerDir.BackColor = Color.Red;
                Packer_AddonBuilder.Enabled = false;
            }
            Properties.Settings.Default.Reload();
        }

        private void button32_Click(object sender, EventArgs e)
        {
            {
                if (ServerDir.Text != "")
                {
                    string dayFolderPath = DayZPath.Text.ToString();
                    Console.WriteLine("\r\ndayFolderPath " + dayFolderPath);
                    string command = @"
:a
cls
@echo off
taskkill /f /im DayZ_x64.exe
timeout 2 > nul
taskkill /f /im DayZServer_x64.exe
pause

start """" """+ServerDir.Text+ @"\DayZServer_x64.exe"" -config=serverDZ_humanity.cfg -server -port=2302 -profiles=" + ServerDir.Text + @"\server_profile -newErrorsAreWarnings=1 -dologs -adminlog -noPause ""-mod=";

                    command += prefixMods.Text.ToString() + "" + DiskPpath.Text.ToString().Replace(@"\", "/") + "@" + modPfolder.Text.ToString() + ";\"";

                    if (useServerMods.Checked)
                    {
                        command += " \"-servermod=" + serverModPrefix.Text.ToString() + "\"";
                    }

                    command += @"

pause
goto a";


                    ConsoleLog.AppendText("\r\nGenerating Server Bat:\r\n" + command);

                    File.WriteAllText(ServerDir.Text + "\\DayZ_Server_" + modPfolder.Text.ToString() + ".bat", command);

                    string filename = ServerDir.Text + "\\DayZ_Server_" + modPfolder.Text.ToString() + ".bat";
                    string parameters = $"/k \"{filename}\"";
                    Process.Start("cmd", parameters);
                }
                else
                {
                    MessageBox.Show("Cannot run server. DayZ Server path is empty.", "Path not found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}
