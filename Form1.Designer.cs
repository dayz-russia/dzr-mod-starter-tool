﻿namespace Mod_Starter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ModsToLoad = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.CreateServerBat = new System.Windows.Forms.Button();
            this.StartWorkbench = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button15 = new System.Windows.Forms.Button();
            this.pDirectories = new System.Windows.Forms.ListBox();
            this.FilterCheckbox = new System.Windows.Forms.CheckBox();
            this.button14 = new System.Windows.Forms.Button();
            this.filesInDir = new System.Windows.Forms.ListBox();
            this.button13 = new System.Windows.Forms.Button();
            this.openAllFiles = new System.Windows.Forms.Button();
            this.openpFolder = new System.Windows.Forms.Button();
            this.startClientBut = new System.Windows.Forms.Button();
            this.CNTscript = new System.Windows.Forms.Button();
            this.SRVscript = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button31 = new System.Windows.Forms.Button();
            this.ServerDir = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.LaunchParams = new System.Windows.Forms.TextBox();
            this.ClnScriptLogPath = new System.Windows.Forms.TextBox();
            this.SrvScriptLogPath = new System.Windows.Forms.TextBox();
            this.useServerMods = new System.Windows.Forms.CheckBox();
            this.packBeforeStart = new System.Windows.Forms.CheckBox();
            this.startBothCheckbox = new System.Windows.Forms.CheckBox();
            this.serverModPrefix = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.killProcesses = new System.Windows.Forms.CheckBox();
            this.button28 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.ToolsDir = new System.Windows.Forms.TextBox();
            this.DayZPath = new System.Windows.Forms.TextBox();
            this.prefixMods = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.playerName = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Packer_MakePbo = new System.Windows.Forms.RadioButton();
            this.Packer_AddonBuilder = new System.Windows.Forms.RadioButton();
            this.Packer_PboProject = new System.Windows.Forms.RadioButton();
            this.button30 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.WipeMissionDiag = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.clearLogs = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.OpenProfiles = new System.Windows.Forms.Button();
            this.CNTcrash = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.CNTrpt = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.SRVcrash = new System.Windows.Forms.Button();
            this.OpenWorkshop = new System.Windows.Forms.Button();
            this.SRVrpt = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.openPackFolder = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.packPbo = new System.Windows.Forms.Button();
            this.CustomFolder = new System.Windows.Forms.TextBox();
            this.CheckBoxPanel = new System.Windows.Forms.Panel();
            this.CoreCheckbox = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pboFolderCheckpbox = new System.Windows.Forms.CheckBox();
            this.TypesFolderCheckbox = new System.Windows.Forms.CheckBox();
            this.modcppCheckbox = new System.Windows.Forms.CheckBox();
            this.GameCheckbox = new System.Windows.Forms.CheckBox();
            this.WorldCheckbox = new System.Windows.Forms.CheckBox();
            this.MissionCheckbox = new System.Windows.Forms.CheckBox();
            this.LanguagaCheckbox = new System.Windows.Forms.CheckBox();
            this.DataCheckbox = new System.Windows.Forms.CheckBox();
            this.GuiCheckbox = new System.Windows.Forms.CheckBox();
            this.createStructure = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.DiskPpath = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PboPrefix = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.requiredAddons = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.isServerMod = new System.Windows.Forms.CheckBox();
            this.modDescription = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.modPfolder = new System.Windows.Forms.TextBox();
            this.modAuthor = new System.Windows.Forms.TextBox();
            this.modName = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.presetName = new System.Windows.Forms.TextBox();
            this.presetsList = new System.Windows.Forms.ComboBox();
            this.RefreshMods = new System.Windows.Forms.Button();
            this.WorkshopMods = new System.Windows.Forms.TreeView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.button6 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ConfigModFolder = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.launchMode = new System.Windows.Forms.Panel();
            this.clientOnly = new System.Windows.Forms.RadioButton();
            this.ServerOnly = new System.Windows.Forms.RadioButton();
            this.ClientServer = new System.Windows.Forms.RadioButton();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.BrowseP = new System.Windows.Forms.FolderBrowserDialog();
            this.BrowseDayZ = new System.Windows.Forms.FolderBrowserDialog();
            this.dataSet1 = new System.Data.DataSet();
            this.ConsoleLog = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.infoIcon = new System.Windows.Forms.Label();
            this.button32 = new System.Windows.Forms.Button();
            this.ModsToLoad.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.CheckBoxPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.launchMode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // ModsToLoad
            // 
            this.ModsToLoad.Controls.Add(this.tabPage1);
            this.ModsToLoad.Controls.Add(this.tabPage4);
            this.ModsToLoad.Controls.Add(this.tabPage2);
            this.ModsToLoad.Controls.Add(this.tabPage3);
            this.ModsToLoad.Location = new System.Drawing.Point(12, 4);
            this.ModsToLoad.Name = "ModsToLoad";
            this.ModsToLoad.SelectedIndex = 0;
            this.ModsToLoad.Size = new System.Drawing.Size(1482, 600);
            this.ModsToLoad.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.button32);
            this.tabPage1.Controls.Add(this.CreateServerBat);
            this.tabPage1.Controls.Add(this.StartWorkbench);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.startClientBut);
            this.tabPage1.Controls.Add(this.CNTscript);
            this.tabPage1.Controls.Add(this.SRVscript);
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1474, 574);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Create Mod";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // CreateServerBat
            // 
            this.CreateServerBat.BackColor = System.Drawing.Color.LimeGreen;
            this.CreateServerBat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CreateServerBat.Location = new System.Drawing.Point(1229, 200);
            this.CreateServerBat.Name = "CreateServerBat";
            this.CreateServerBat.Size = new System.Drawing.Size(170, 23);
            this.CreateServerBat.TabIndex = 48;
            this.CreateServerBat.Text = "Create DayZ Server Bat";
            this.CreateServerBat.UseVisualStyleBackColor = false;
            this.CreateServerBat.Click += new System.EventHandler(this.CreateServerBat_Click);
            // 
            // StartWorkbench
            // 
            this.StartWorkbench.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StartWorkbench.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StartWorkbench.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.StartWorkbench.Location = new System.Drawing.Point(1228, 83);
            this.StartWorkbench.Name = "StartWorkbench";
            this.StartWorkbench.Size = new System.Drawing.Size(223, 34);
            this.StartWorkbench.TabIndex = 46;
            this.StartWorkbench.Text = "START WORKBENCH";
            this.StartWorkbench.UseVisualStyleBackColor = false;
            this.StartWorkbench.Click += new System.EventHandler(this.StartWorkbench_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.button15);
            this.panel1.Controls.Add(this.pDirectories);
            this.panel1.Controls.Add(this.FilterCheckbox);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.filesInDir);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Controls.Add(this.openAllFiles);
            this.panel1.Controls.Add(this.openpFolder);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(532, 492);
            this.panel1.TabIndex = 45;
            // 
            // button15
            // 
            this.button15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button15.Location = new System.Drawing.Point(3, 432);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 20);
            this.button15.TabIndex = 45;
            this.button15.Text = "Refresh";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // pDirectories
            // 
            this.pDirectories.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pDirectories.FormattingEnabled = true;
            this.pDirectories.HorizontalScrollbar = true;
            this.pDirectories.Location = new System.Drawing.Point(2, 6);
            this.pDirectories.Name = "pDirectories";
            this.pDirectories.ScrollAlwaysVisible = true;
            this.pDirectories.Size = new System.Drawing.Size(172, 420);
            this.pDirectories.TabIndex = 31;
            this.pDirectories.SelectedIndexChanged += new System.EventHandler(this.pDirectories_SelectedIndexChanged);
            this.pDirectories.DoubleClick += new System.EventHandler(this.pDirectories_DoubleClick);
            this.pDirectories.MouseEnter += new System.EventHandler(this.pDirectories_MouseEnter);
            // 
            // FilterCheckbox
            // 
            this.FilterCheckbox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.FilterCheckbox.AutoSize = true;
            this.FilterCheckbox.Checked = global::Mod_Starter.Properties.Settings.Default.FilterFiles;
            this.FilterCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FilterCheckbox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "FilterFiles", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.FilterCheckbox.Location = new System.Drawing.Point(182, 432);
            this.FilterCheckbox.Name = "FilterCheckbox";
            this.FilterCheckbox.Size = new System.Drawing.Size(113, 17);
            this.FilterCheckbox.TabIndex = 42;
            this.FilterCheckbox.Text = "Filter non-readable";
            this.FilterCheckbox.UseVisualStyleBackColor = true;
            this.FilterCheckbox.CheckedChanged += new System.EventHandler(this.FilterCheckbox_CheckedChanged);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.DarkRed;
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Location = new System.Drawing.Point(178, 458);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 44;
            this.button14.Text = "Delete File";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // filesInDir
            // 
            this.filesInDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.filesInDir.FormattingEnabled = true;
            this.filesInDir.HorizontalScrollbar = true;
            this.filesInDir.Location = new System.Drawing.Point(178, 6);
            this.filesInDir.Name = "filesInDir";
            this.filesInDir.ScrollAlwaysVisible = true;
            this.filesInDir.Size = new System.Drawing.Size(351, 420);
            this.filesInDir.TabIndex = 32;
            this.filesInDir.SelectedIndexChanged += new System.EventHandler(this.filesInDir_SelectedIndexChanged);
            this.filesInDir.DoubleClick += new System.EventHandler(this.filesInDir_DoubleClick);
            // 
            // button13
            // 
            this.button13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button13.BackColor = System.Drawing.Color.DarkRed;
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(3, 456);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 43;
            this.button13.Text = "Delete Dir";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // openAllFiles
            // 
            this.openAllFiles.Location = new System.Drawing.Point(395, 432);
            this.openAllFiles.Name = "openAllFiles";
            this.openAllFiles.Size = new System.Drawing.Size(134, 49);
            this.openAllFiles.TabIndex = 27;
            this.openAllFiles.Text = "Open All Files";
            this.openAllFiles.UseVisualStyleBackColor = true;
            this.openAllFiles.Click += new System.EventHandler(this.openAllFiles_Click);
            // 
            // openpFolder
            // 
            this.openpFolder.Location = new System.Drawing.Point(84, 431);
            this.openpFolder.Name = "openpFolder";
            this.openpFolder.Size = new System.Drawing.Size(92, 50);
            this.openpFolder.TabIndex = 10;
            this.openpFolder.Text = "Open Folder";
            this.openpFolder.UseVisualStyleBackColor = true;
            this.openpFolder.Click += new System.EventHandler(this.openpFolder_Click);
            // 
            // startClientBut
            // 
            this.startClientBut.BackColor = System.Drawing.Color.PaleTurquoise;
            this.startClientBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startClientBut.Location = new System.Drawing.Point(1228, 47);
            this.startClientBut.Name = "startClientBut";
            this.startClientBut.Size = new System.Drawing.Size(223, 34);
            this.startClientBut.TabIndex = 39;
            this.startClientBut.Text = "Start  Client";
            this.startClientBut.UseVisualStyleBackColor = false;
            this.startClientBut.Click += new System.EventHandler(this.startClientBut_Click);
            // 
            // CNTscript
            // 
            this.CNTscript.BackColor = System.Drawing.Color.DarkGray;
            this.CNTscript.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CNTscript.Location = new System.Drawing.Point(1228, 154);
            this.CNTscript.Name = "CNTscript";
            this.CNTscript.Size = new System.Drawing.Size(223, 21);
            this.CNTscript.TabIndex = 36;
            this.CNTscript.Text = "Client Script Log";
            this.CNTscript.UseVisualStyleBackColor = false;
            this.CNTscript.Click += new System.EventHandler(this.CNTscript_Click);
            // 
            // SRVscript
            // 
            this.SRVscript.BackColor = System.Drawing.Color.DarkGray;
            this.SRVscript.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SRVscript.Location = new System.Drawing.Point(1228, 177);
            this.SRVscript.Name = "SRVscript";
            this.SRVscript.Size = new System.Drawing.Size(223, 21);
            this.SRVscript.TabIndex = 33;
            this.SRVscript.Text = "Server Script Log";
            this.SRVscript.UseVisualStyleBackColor = false;
            this.SRVscript.Click += new System.EventHandler(this.button12_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Maroon;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(1228, 119);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(223, 33);
            this.button7.TabIndex = 28;
            this.button7.Text = "Kill server + client";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.infoIcon);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.button31);
            this.groupBox3.Controls.Add(this.ServerDir);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.LaunchParams);
            this.groupBox3.Controls.Add(this.ClnScriptLogPath);
            this.groupBox3.Controls.Add(this.SrvScriptLogPath);
            this.groupBox3.Controls.Add(this.useServerMods);
            this.groupBox3.Controls.Add(this.packBeforeStart);
            this.groupBox3.Controls.Add(this.startBothCheckbox);
            this.groupBox3.Controls.Add(this.serverModPrefix);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.killProcesses);
            this.groupBox3.Controls.Add(this.button28);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.ToolsDir);
            this.groupBox3.Controls.Add(this.DayZPath);
            this.groupBox3.Controls.Add(this.prefixMods);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.playerName);
            this.groupBox3.Location = new System.Drawing.Point(992, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(232, 541);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Launch Settings";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 98);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(99, 13);
            this.label18.TabIndex = 35;
            this.label18.Text = "DayZ Server Folder";
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(144, 112);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(75, 23);
            this.button31.TabIndex = 37;
            this.button31.Text = "Browse";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // ServerDir
            // 
            this.ServerDir.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "ServerDir", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ServerDir.Location = new System.Drawing.Point(6, 114);
            this.ServerDir.Name = "ServerDir";
            this.ServerDir.Size = new System.Drawing.Size(128, 20);
            this.ServerDir.TabIndex = 36;
            this.ServerDir.Text = global::Mod_Starter.Properties.Settings.Default.ServerDir;
            this.ServerDir.TextChanged += new System.EventHandler(this.ServerDir_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 166);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 33;
            this.label16.Text = "-params";
            // 
            // LaunchParams
            // 
            this.LaunchParams.AcceptsReturn = true;
            this.LaunchParams.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "LaunchParam", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.LaunchParams.Location = new System.Drawing.Point(9, 180);
            this.LaunchParams.Name = "LaunchParams";
            this.LaunchParams.Size = new System.Drawing.Size(208, 20);
            this.LaunchParams.TabIndex = 34;
            this.LaunchParams.Text = global::Mod_Starter.Properties.Settings.Default.LaunchParam;
            // 
            // ClnScriptLogPath
            // 
            this.ClnScriptLogPath.Location = new System.Drawing.Point(117, 387);
            this.ClnScriptLogPath.Name = "ClnScriptLogPath";
            this.ClnScriptLogPath.Size = new System.Drawing.Size(100, 20);
            this.ClnScriptLogPath.TabIndex = 32;
            this.ClnScriptLogPath.Visible = false;
            // 
            // SrvScriptLogPath
            // 
            this.SrvScriptLogPath.Location = new System.Drawing.Point(117, 366);
            this.SrvScriptLogPath.Name = "SrvScriptLogPath";
            this.SrvScriptLogPath.Size = new System.Drawing.Size(100, 20);
            this.SrvScriptLogPath.TabIndex = 31;
            this.SrvScriptLogPath.Visible = false;
            // 
            // useServerMods
            // 
            this.useServerMods.AutoSize = true;
            this.useServerMods.Checked = global::Mod_Starter.Properties.Settings.Default.UseServerModsLine;
            this.useServerMods.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "UseServerModsLine", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.useServerMods.Location = new System.Drawing.Point(9, 328);
            this.useServerMods.Name = "useServerMods";
            this.useServerMods.Size = new System.Drawing.Size(104, 17);
            this.useServerMods.TabIndex = 30;
            this.useServerMods.Text = "Use Servermods";
            this.useServerMods.UseVisualStyleBackColor = true;
            this.useServerMods.CheckStateChanged += new System.EventHandler(this.useServerMods_CheckStateChanged);
            // 
            // packBeforeStart
            // 
            this.packBeforeStart.AutoSize = true;
            this.packBeforeStart.Checked = global::Mod_Starter.Properties.Settings.Default.PackCheck;
            this.packBeforeStart.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "PackCheck", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.packBeforeStart.Location = new System.Drawing.Point(9, 368);
            this.packBeforeStart.Name = "packBeforeStart";
            this.packBeforeStart.Size = new System.Drawing.Size(51, 17);
            this.packBeforeStart.TabIndex = 29;
            this.packBeforeStart.Text = "Pack";
            this.packBeforeStart.UseVisualStyleBackColor = true;
            this.packBeforeStart.CheckedChanged += new System.EventHandler(this.packBeforeStart_CheckedChanged);
            this.packBeforeStart.CheckStateChanged += new System.EventHandler(this.packBeforeStart_CheckStateChanged);
            // 
            // startBothCheckbox
            // 
            this.startBothCheckbox.AutoSize = true;
            this.startBothCheckbox.Location = new System.Drawing.Point(9, 385);
            this.startBothCheckbox.Name = "startBothCheckbox";
            this.startBothCheckbox.Size = new System.Drawing.Size(73, 17);
            this.startBothCheckbox.TabIndex = 28;
            this.startBothCheckbox.Text = "Start Both";
            this.startBothCheckbox.UseVisualStyleBackColor = true;
            this.startBothCheckbox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // serverModPrefix
            // 
            this.serverModPrefix.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "serverModPrefix", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.serverModPrefix.Location = new System.Drawing.Point(9, 304);
            this.serverModPrefix.Name = "serverModPrefix";
            this.serverModPrefix.Size = new System.Drawing.Size(208, 20);
            this.serverModPrefix.TabIndex = 27;
            this.serverModPrefix.Text = global::Mod_Starter.Properties.Settings.Default.serverModPrefix;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 290);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "-servermod prefix for BAT";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(94, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "DayZ Tools Folder";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "DayZ Folder";
            // 
            // killProcesses
            // 
            this.killProcesses.AutoSize = true;
            this.killProcesses.Checked = global::Mod_Starter.Properties.Settings.Default.killOnLaunch;
            this.killProcesses.CheckState = System.Windows.Forms.CheckState.Checked;
            this.killProcesses.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "killOnLaunch", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.killProcesses.Location = new System.Drawing.Point(9, 351);
            this.killProcesses.Name = "killProcesses";
            this.killProcesses.Size = new System.Drawing.Size(179, 17);
            this.killProcesses.TabIndex = 23;
            this.killProcesses.Text = "kill DayZDiag_x64.exe on restart";
            this.killProcesses.UseVisualStyleBackColor = true;
            this.killProcesses.CheckStateChanged += new System.EventHandler(this.killProcesses_CheckStateChanged);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(144, 70);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(75, 23);
            this.button28.TabIndex = 25;
            this.button28.Text = "Browse";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(144, 28);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 25;
            this.button4.Text = "Browse";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // ToolsDir
            // 
            this.ToolsDir.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "ToolsDir", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ToolsDir.Location = new System.Drawing.Point(6, 72);
            this.ToolsDir.Name = "ToolsDir";
            this.ToolsDir.Size = new System.Drawing.Size(128, 20);
            this.ToolsDir.TabIndex = 24;
            this.ToolsDir.Text = global::Mod_Starter.Properties.Settings.Default.ToolsDir;
            this.ToolsDir.TextChanged += new System.EventHandler(this.ToolsDir_TextChanged);
            // 
            // DayZPath
            // 
            this.DayZPath.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "DayZFolder", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DayZPath.Location = new System.Drawing.Point(6, 30);
            this.DayZPath.Name = "DayZPath";
            this.DayZPath.Size = new System.Drawing.Size(128, 20);
            this.DayZPath.TabIndex = 24;
            this.DayZPath.Text = global::Mod_Starter.Properties.Settings.Default.DayZFolder;
            this.DayZPath.TextChanged += new System.EventHandler(this.DayZPath_TextChanged);
            // 
            // prefixMods
            // 
            this.prefixMods.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "prefixMods", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.prefixMods.Location = new System.Drawing.Point(9, 259);
            this.prefixMods.Name = "prefixMods";
            this.prefixMods.Size = new System.Drawing.Size(208, 20);
            this.prefixMods.TabIndex = 13;
            this.prefixMods.Text = global::Mod_Starter.Properties.Settings.Default.prefixMods;
            this.prefixMods.TextChanged += new System.EventHandler(this.prefixMods_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 203);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "-name= for client";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "-mod prefix for BAT";
            // 
            // playerName
            // 
            this.playerName.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "playerName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.playerName.Location = new System.Drawing.Point(9, 217);
            this.playerName.Name = "playerName";
            this.playerName.Size = new System.Drawing.Size(208, 20);
            this.playerName.TabIndex = 20;
            this.playerName.Text = global::Mod_Starter.Properties.Settings.Default.playerName;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.button30);
            this.groupBox2.Controls.Add(this.button29);
            this.groupBox2.Controls.Add(this.WipeMissionDiag);
            this.groupBox2.Controls.Add(this.button21);
            this.groupBox2.Controls.Add(this.button12);
            this.groupBox2.Controls.Add(this.button20);
            this.groupBox2.Controls.Add(this.clearLogs);
            this.groupBox2.Controls.Add(this.button19);
            this.groupBox2.Controls.Add(this.OpenProfiles);
            this.groupBox2.Controls.Add(this.CNTcrash);
            this.groupBox2.Controls.Add(this.button18);
            this.groupBox2.Controls.Add(this.CNTrpt);
            this.groupBox2.Controls.Add(this.button17);
            this.groupBox2.Controls.Add(this.SRVcrash);
            this.groupBox2.Controls.Add(this.OpenWorkshop);
            this.groupBox2.Controls.Add(this.SRVrpt);
            this.groupBox2.Controls.Add(this.button16);
            this.groupBox2.Controls.Add(this.button8);
            this.groupBox2.Controls.Add(this.button11);
            this.groupBox2.Controls.Add(this.openPackFolder);
            this.groupBox2.Controls.Add(this.button10);
            this.groupBox2.Controls.Add(this.packPbo);
            this.groupBox2.Controls.Add(this.CustomFolder);
            this.groupBox2.Controls.Add(this.CheckBoxPanel);
            this.groupBox2.Controls.Add(this.createStructure);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.DiskPpath);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Location = new System.Drawing.Point(538, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(457, 547);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Disk P";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Packer_MakePbo);
            this.groupBox5.Controls.Add(this.Packer_AddonBuilder);
            this.groupBox5.Controls.Add(this.Packer_PboProject);
            this.groupBox5.Location = new System.Drawing.Point(334, 241);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(105, 79);
            this.groupBox5.TabIndex = 55;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Packer:";
            this.groupBox5.TabIndexChanged += new System.EventHandler(this.groupBox5_TabIndexChanged);
            // 
            // Packer_MakePbo
            // 
            this.Packer_MakePbo.AutoSize = true;
            this.Packer_MakePbo.Checked = global::Mod_Starter.Properties.Settings.Default.MakePbo;
            this.Packer_MakePbo.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "MakePbo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Packer_MakePbo.Location = new System.Drawing.Point(6, 20);
            this.Packer_MakePbo.Name = "Packer_MakePbo";
            this.Packer_MakePbo.Size = new System.Drawing.Size(71, 17);
            this.Packer_MakePbo.TabIndex = 11;
            this.Packer_MakePbo.TabStop = true;
            this.Packer_MakePbo.Text = "MakePbo";
            this.Packer_MakePbo.UseVisualStyleBackColor = true;
            this.Packer_MakePbo.CheckedChanged += new System.EventHandler(this.Packer_MakePbo_CheckedChanged);
            this.Packer_MakePbo.Click += new System.EventHandler(this.Packer_MakePbo_Click);
            // 
            // Packer_AddonBuilder
            // 
            this.Packer_AddonBuilder.AutoSize = true;
            this.Packer_AddonBuilder.Checked = global::Mod_Starter.Properties.Settings.Default.AddonBuilder;
            this.Packer_AddonBuilder.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "AddonBuilder", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Packer_AddonBuilder.Location = new System.Drawing.Point(6, 58);
            this.Packer_AddonBuilder.Name = "Packer_AddonBuilder";
            this.Packer_AddonBuilder.Size = new System.Drawing.Size(91, 17);
            this.Packer_AddonBuilder.TabIndex = 54;
            this.Packer_AddonBuilder.Text = "Addon Builder";
            this.Packer_AddonBuilder.UseVisualStyleBackColor = true;
            this.Packer_AddonBuilder.CheckedChanged += new System.EventHandler(this.Packer_AddonBuilder_CheckedChanged);
            // 
            // Packer_PboProject
            // 
            this.Packer_PboProject.AutoSize = true;
            this.Packer_PboProject.Checked = global::Mod_Starter.Properties.Settings.Default.PboProject;
            this.Packer_PboProject.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "PboProject", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Packer_PboProject.Location = new System.Drawing.Point(6, 39);
            this.Packer_PboProject.Name = "Packer_PboProject";
            this.Packer_PboProject.Size = new System.Drawing.Size(77, 17);
            this.Packer_PboProject.TabIndex = 53;
            this.Packer_PboProject.Text = "PboProject";
            this.Packer_PboProject.UseVisualStyleBackColor = true;
            this.Packer_PboProject.CheckedChanged += new System.EventHandler(this.Packer_PboProject_CheckedChanged);
            // 
            // button30
            // 
            this.button30.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button30.Location = new System.Drawing.Point(115, 474);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(75, 24);
            this.button30.TabIndex = 52;
            this.button30.Text = "Bat files";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button29
            // 
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button29.Location = new System.Drawing.Point(6, 474);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(75, 24);
            this.button29.TabIndex = 52;
            this.button29.Text = "serverDZ.cfg";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // WipeMissionDiag
            // 
            this.WipeMissionDiag.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.WipeMissionDiag.BackColor = System.Drawing.Color.DarkRed;
            this.WipeMissionDiag.ForeColor = System.Drawing.Color.White;
            this.WipeMissionDiag.Location = new System.Drawing.Point(6, 521);
            this.WipeMissionDiag.Name = "WipeMissionDiag";
            this.WipeMissionDiag.Size = new System.Drawing.Size(438, 22);
            this.WipeMissionDiag.TabIndex = 46;
            this.WipeMissionDiag.Text = "Wipe dayzOffline.chernarusplus storages";
            this.WipeMissionDiag.UseVisualStyleBackColor = false;
            this.WipeMissionDiag.Click += new System.EventHandler(this.button27_Click);
            // 
            // button21
            // 
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button21.Location = new System.Drawing.Point(115, 444);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 24);
            this.button21.TabIndex = 51;
            this.button21.Text = "globals.xml";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Khaki;
            this.button12.Location = new System.Drawing.Point(225, 551);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(107, 21);
            this.button12.TabIndex = 41;
            this.button12.Text = "Clear Client Logs";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click_1);
            // 
            // button20
            // 
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button20.Location = new System.Drawing.Point(115, 416);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 24);
            this.button20.TabIndex = 50;
            this.button20.Text = "init.c";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // clearLogs
            // 
            this.clearLogs.BackColor = System.Drawing.Color.Khaki;
            this.clearLogs.Location = new System.Drawing.Point(338, 551);
            this.clearLogs.Name = "clearLogs";
            this.clearLogs.Size = new System.Drawing.Size(107, 23);
            this.clearLogs.TabIndex = 40;
            this.clearLogs.Text = "Clear Server Logs";
            this.clearLogs.UseVisualStyleBackColor = false;
            this.clearLogs.Click += new System.EventHandler(this.clearLogs_Click);
            // 
            // button19
            // 
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button19.Location = new System.Drawing.Point(338, 416);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(105, 24);
            this.button19.TabIndex = 47;
            this.button19.Text = "Server Profiles";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // OpenProfiles
            // 
            this.OpenProfiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OpenProfiles.Location = new System.Drawing.Point(6, 444);
            this.OpenProfiles.Name = "OpenProfiles";
            this.OpenProfiles.Size = new System.Drawing.Size(75, 24);
            this.OpenProfiles.TabIndex = 49;
            this.OpenProfiles.Text = "mpmissions";
            this.OpenProfiles.UseVisualStyleBackColor = true;
            this.OpenProfiles.Click += new System.EventHandler(this.OpenProfiles_Click);
            // 
            // CNTcrash
            // 
            this.CNTcrash.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CNTcrash.Location = new System.Drawing.Point(225, 469);
            this.CNTcrash.Name = "CNTcrash";
            this.CNTcrash.Size = new System.Drawing.Size(107, 21);
            this.CNTcrash.TabIndex = 38;
            this.CNTcrash.Text = "Client Crash Log";
            this.CNTcrash.UseVisualStyleBackColor = true;
            this.CNTcrash.Click += new System.EventHandler(this.CNTcrash_Click);
            // 
            // button18
            // 
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button18.Location = new System.Drawing.Point(225, 416);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(107, 24);
            this.button18.TabIndex = 46;
            this.button18.Text = "Client Profiles";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // CNTrpt
            // 
            this.CNTrpt.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CNTrpt.Location = new System.Drawing.Point(225, 444);
            this.CNTrpt.Name = "CNTrpt";
            this.CNTrpt.Size = new System.Drawing.Size(107, 21);
            this.CNTrpt.TabIndex = 37;
            this.CNTrpt.Text = "Client RPT";
            this.CNTrpt.UseVisualStyleBackColor = true;
            this.CNTrpt.Click += new System.EventHandler(this.CNTrpt_Click);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.LimeGreen;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button17.Location = new System.Drawing.Point(104, 326);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(72, 23);
            this.button17.TabIndex = 47;
            this.button17.Text = "TG Commit";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // SRVcrash
            // 
            this.SRVcrash.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SRVcrash.Location = new System.Drawing.Point(338, 469);
            this.SRVcrash.Name = "SRVcrash";
            this.SRVcrash.Size = new System.Drawing.Size(105, 21);
            this.SRVcrash.TabIndex = 35;
            this.SRVcrash.Text = "Server Crash Log";
            this.SRVcrash.UseVisualStyleBackColor = true;
            this.SRVcrash.Click += new System.EventHandler(this.SRVcrash_Click);
            // 
            // OpenWorkshop
            // 
            this.OpenWorkshop.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OpenWorkshop.Location = new System.Drawing.Point(6, 416);
            this.OpenWorkshop.Name = "OpenWorkshop";
            this.OpenWorkshop.Size = new System.Drawing.Size(75, 24);
            this.OpenWorkshop.TabIndex = 48;
            this.OpenWorkshop.Text = "workshop";
            this.OpenWorkshop.UseVisualStyleBackColor = true;
            this.OpenWorkshop.Click += new System.EventHandler(this.OpenWorkshop_Click);
            // 
            // SRVrpt
            // 
            this.SRVrpt.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SRVrpt.Location = new System.Drawing.Point(338, 444);
            this.SRVrpt.Name = "SRVrpt";
            this.SRVrpt.Size = new System.Drawing.Size(105, 21);
            this.SRVrpt.TabIndex = 34;
            this.SRVrpt.Text = "Server RPT";
            this.SRVrpt.UseVisualStyleBackColor = true;
            this.SRVrpt.Click += new System.EventHandler(this.SRVrpt_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button16.Location = new System.Drawing.Point(104, 297);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(72, 23);
            this.button16.TabIndex = 46;
            this.button16.Text = "TG Pull";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(6, 379);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(438, 23);
            this.button8.TabIndex = 26;
            this.button8.Text = "Create Custom Structure";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button11.Location = new System.Drawing.Point(338, 494);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(105, 21);
            this.button11.TabIndex = 30;
            this.button11.Text = "Server Logs Dir";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // openPackFolder
            // 
            this.openPackFolder.Location = new System.Drawing.Point(225, 326);
            this.openPackFolder.Name = "openPackFolder";
            this.openPackFolder.Size = new System.Drawing.Size(92, 23);
            this.openPackFolder.TabIndex = 11;
            this.openPackFolder.Text = "Open @Folder";
            this.openPackFolder.UseVisualStyleBackColor = true;
            this.openPackFolder.Click += new System.EventHandler(this.openPackFolder_Click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button10.Location = new System.Drawing.Point(225, 494);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(107, 21);
            this.button10.TabIndex = 29;
            this.button10.Text = "Client Logs Dir";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // packPbo
            // 
            this.packPbo.Location = new System.Drawing.Point(334, 326);
            this.packPbo.Name = "packPbo";
            this.packPbo.Size = new System.Drawing.Size(105, 23);
            this.packPbo.TabIndex = 24;
            this.packPbo.Text = "Pack Pbo";
            this.packPbo.UseVisualStyleBackColor = true;
            this.packPbo.Click += new System.EventHandler(this.button8_Click);
            // 
            // CustomFolder
            // 
            this.CustomFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CustomFolder.Location = new System.Drawing.Point(6, 355);
            this.CustomFolder.Name = "CustomFolder";
            this.CustomFolder.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CustomFolder.Size = new System.Drawing.Size(437, 18);
            this.CustomFolder.TabIndex = 25;
            this.CustomFolder.Text = "4_World\\Entities\\manbase\\PlayerBase.c";
            // 
            // CheckBoxPanel
            // 
            this.CheckBoxPanel.Controls.Add(this.CoreCheckbox);
            this.CheckBoxPanel.Controls.Add(this.label11);
            this.CheckBoxPanel.Controls.Add(this.pboFolderCheckpbox);
            this.CheckBoxPanel.Controls.Add(this.TypesFolderCheckbox);
            this.CheckBoxPanel.Controls.Add(this.modcppCheckbox);
            this.CheckBoxPanel.Controls.Add(this.GameCheckbox);
            this.CheckBoxPanel.Controls.Add(this.WorldCheckbox);
            this.CheckBoxPanel.Controls.Add(this.MissionCheckbox);
            this.CheckBoxPanel.Controls.Add(this.LanguagaCheckbox);
            this.CheckBoxPanel.Controls.Add(this.DataCheckbox);
            this.CheckBoxPanel.Controls.Add(this.GuiCheckbox);
            this.CheckBoxPanel.Location = new System.Drawing.Point(296, 11);
            this.CheckBoxPanel.Name = "CheckBoxPanel";
            this.CheckBoxPanel.Size = new System.Drawing.Size(147, 227);
            this.CheckBoxPanel.TabIndex = 23;
            // 
            // CoreCheckbox
            // 
            this.CoreCheckbox.AutoSize = true;
            this.CoreCheckbox.Checked = true;
            this.CoreCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CoreCheckbox.Enabled = false;
            this.CoreCheckbox.Location = new System.Drawing.Point(8, 4);
            this.CoreCheckbox.Name = "CoreCheckbox";
            this.CoreCheckbox.Size = new System.Drawing.Size(60, 17);
            this.CoreCheckbox.TabIndex = 10;
            this.CoreCheckbox.Text = "1_Core";
            this.CoreCheckbox.UseVisualStyleBackColor = true;
            this.CoreCheckbox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // label11
            // 
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Location = new System.Drawing.Point(8, 149);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 2);
            this.label11.TabIndex = 9;
            // 
            // pboFolderCheckpbox
            // 
            this.pboFolderCheckpbox.AutoSize = true;
            this.pboFolderCheckpbox.Checked = true;
            this.pboFolderCheckpbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.pboFolderCheckpbox.Location = new System.Drawing.Point(8, 160);
            this.pboFolderCheckpbox.Name = "pboFolderCheckpbox";
            this.pboFolderCheckpbox.Size = new System.Drawing.Size(135, 17);
            this.pboFolderCheckpbox.TabIndex = 6;
            this.pboFolderCheckpbox.Text = "Prepare Folder for PBO";
            this.pboFolderCheckpbox.UseVisualStyleBackColor = true;
            // 
            // TypesFolderCheckbox
            // 
            this.TypesFolderCheckbox.AutoSize = true;
            this.TypesFolderCheckbox.Checked = true;
            this.TypesFolderCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TypesFolderCheckbox.Location = new System.Drawing.Point(8, 206);
            this.TypesFolderCheckbox.Name = "TypesFolderCheckbox";
            this.TypesFolderCheckbox.Size = new System.Drawing.Size(84, 17);
            this.TypesFolderCheckbox.TabIndex = 8;
            this.TypesFolderCheckbox.Text = "Types folder";
            this.TypesFolderCheckbox.UseVisualStyleBackColor = true;
            // 
            // modcppCheckbox
            // 
            this.modcppCheckbox.AutoSize = true;
            this.modcppCheckbox.Checked = true;
            this.modcppCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.modcppCheckbox.Location = new System.Drawing.Point(8, 183);
            this.modcppCheckbox.Name = "modcppCheckbox";
            this.modcppCheckbox.Size = new System.Drawing.Size(102, 17);
            this.modcppCheckbox.TabIndex = 7;
            this.modcppCheckbox.Text = "default mod.cpp";
            this.modcppCheckbox.UseVisualStyleBackColor = true;
            // 
            // GameCheckbox
            // 
            this.GameCheckbox.AutoSize = true;
            this.GameCheckbox.Checked = true;
            this.GameCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GameCheckbox.Location = new System.Drawing.Point(8, 23);
            this.GameCheckbox.Name = "GameCheckbox";
            this.GameCheckbox.Size = new System.Drawing.Size(66, 17);
            this.GameCheckbox.TabIndex = 0;
            this.GameCheckbox.Text = "3_Game";
            this.GameCheckbox.UseVisualStyleBackColor = true;
            this.GameCheckbox.CheckedChanged += new System.EventHandler(this.GameCheckbox_CheckedChanged);
            // 
            // WorldCheckbox
            // 
            this.WorldCheckbox.AutoSize = true;
            this.WorldCheckbox.Checked = true;
            this.WorldCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.WorldCheckbox.Location = new System.Drawing.Point(8, 42);
            this.WorldCheckbox.Name = "WorldCheckbox";
            this.WorldCheckbox.Size = new System.Drawing.Size(66, 17);
            this.WorldCheckbox.TabIndex = 1;
            this.WorldCheckbox.Text = "4_World";
            this.WorldCheckbox.UseVisualStyleBackColor = true;
            this.WorldCheckbox.CheckedChanged += new System.EventHandler(this.WorldCheckbox_CheckedChanged);
            // 
            // MissionCheckbox
            // 
            this.MissionCheckbox.AutoSize = true;
            this.MissionCheckbox.Checked = true;
            this.MissionCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MissionCheckbox.Location = new System.Drawing.Point(8, 61);
            this.MissionCheckbox.Name = "MissionCheckbox";
            this.MissionCheckbox.Size = new System.Drawing.Size(73, 17);
            this.MissionCheckbox.TabIndex = 2;
            this.MissionCheckbox.Text = "5_Mission";
            this.MissionCheckbox.UseVisualStyleBackColor = true;
            this.MissionCheckbox.CheckedChanged += new System.EventHandler(this.MissionCheckbox_CheckedChanged);
            // 
            // LanguagaCheckbox
            // 
            this.LanguagaCheckbox.AutoSize = true;
            this.LanguagaCheckbox.Checked = true;
            this.LanguagaCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.LanguagaCheckbox.Location = new System.Drawing.Point(8, 118);
            this.LanguagaCheckbox.Name = "LanguagaCheckbox";
            this.LanguagaCheckbox.Size = new System.Drawing.Size(91, 17);
            this.LanguagaCheckbox.TabIndex = 5;
            this.LanguagaCheckbox.Text = "languagecore";
            this.LanguagaCheckbox.UseVisualStyleBackColor = true;
            // 
            // DataCheckbox
            // 
            this.DataCheckbox.AutoSize = true;
            this.DataCheckbox.Checked = true;
            this.DataCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DataCheckbox.Location = new System.Drawing.Point(8, 80);
            this.DataCheckbox.Name = "DataCheckbox";
            this.DataCheckbox.Size = new System.Drawing.Size(49, 17);
            this.DataCheckbox.TabIndex = 3;
            this.DataCheckbox.Text = "Data";
            this.DataCheckbox.UseVisualStyleBackColor = true;
            // 
            // GuiCheckbox
            // 
            this.GuiCheckbox.AutoSize = true;
            this.GuiCheckbox.Checked = true;
            this.GuiCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GuiCheckbox.Location = new System.Drawing.Point(8, 99);
            this.GuiCheckbox.Name = "GuiCheckbox";
            this.GuiCheckbox.Size = new System.Drawing.Size(45, 17);
            this.GuiCheckbox.TabIndex = 4;
            this.GuiCheckbox.Text = "GUI";
            this.GuiCheckbox.UseVisualStyleBackColor = true;
            // 
            // createStructure
            // 
            this.createStructure.Location = new System.Drawing.Point(6, 297);
            this.createStructure.Name = "createStructure";
            this.createStructure.Size = new System.Drawing.Size(92, 52);
            this.createStructure.TabIndex = 3;
            this.createStructure.Text = "Create structure";
            this.createStructure.UseVisualStyleBackColor = true;
            this.createStructure.Click += new System.EventHandler(this.createStructure_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Disk P path";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(196, 9);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Browse";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // DiskPpath
            // 
            this.DiskPpath.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "DiskPPath", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DiskPpath.Location = new System.Drawing.Point(76, 11);
            this.DiskPpath.Name = "DiskPpath";
            this.DiskPpath.Size = new System.Drawing.Size(114, 20);
            this.DiskPpath.TabIndex = 0;
            this.DiskPpath.Text = global::Mod_Starter.Properties.Settings.Default.DiskPPath;
            this.DiskPpath.TextChanged += new System.EventHandler(this.DiskPpath_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PboPrefix);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.requiredAddons);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.isServerMod);
            this.groupBox1.Controls.Add(this.modDescription);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.modPfolder);
            this.groupBox1.Controls.Add(this.modAuthor);
            this.groupBox1.Controls.Add(this.modName);
            this.groupBox1.Location = new System.Drawing.Point(6, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(284, 252);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "config.cpp";
            // 
            // PboPrefix
            // 
            this.PboPrefix.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "PboPrefix", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PboPrefix.Location = new System.Drawing.Point(85, 206);
            this.PboPrefix.Name = "PboPrefix";
            this.PboPrefix.Size = new System.Drawing.Size(193, 20);
            this.PboPrefix.TabIndex = 26;
            this.PboPrefix.Text = global::Mod_Starter.Properties.Settings.Default.PboPrefix;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(2, 209);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "$PBOPREFIX$";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // requiredAddons
            // 
            this.requiredAddons.Enabled = false;
            this.requiredAddons.Location = new System.Drawing.Point(85, 160);
            this.requiredAddons.Name = "requiredAddons";
            this.requiredAddons.Size = new System.Drawing.Size(191, 20);
            this.requiredAddons.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(13, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mod Folder";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "dependencies";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Mod Author";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mod Title";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // isServerMod
            // 
            this.isServerMod.AutoSize = true;
            this.isServerMod.Checked = global::Mod_Starter.Properties.Settings.Default.isServerMod;
            this.isServerMod.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "isServerMod", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.isServerMod.Location = new System.Drawing.Point(85, 186);
            this.isServerMod.Name = "isServerMod";
            this.isServerMod.Size = new System.Drawing.Size(75, 17);
            this.isServerMod.TabIndex = 24;
            this.isServerMod.Text = "servermod";
            this.isServerMod.UseVisualStyleBackColor = true;
            this.isServerMod.CheckedChanged += new System.EventHandler(this.isServerMod_CheckedChanged);
            // 
            // modDescription
            // 
            this.modDescription.Location = new System.Drawing.Point(85, 76);
            this.modDescription.Name = "modDescription";
            this.modDescription.Size = new System.Drawing.Size(191, 52);
            this.modDescription.TabIndex = 2;
            this.modDescription.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Mod\r\nDescription";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // modPfolder
            // 
            this.modPfolder.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "currentPfolder", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.modPfolder.Location = new System.Drawing.Point(85, 24);
            this.modPfolder.Name = "modPfolder";
            this.modPfolder.Size = new System.Drawing.Size(191, 20);
            this.modPfolder.TabIndex = 0;
            this.modPfolder.Text = global::Mod_Starter.Properties.Settings.Default.currentPfolder;
            this.modPfolder.KeyUp += new System.Windows.Forms.KeyEventHandler(this.modPfolder_KeyUp);
            // 
            // modAuthor
            // 
            this.modAuthor.Location = new System.Drawing.Point(85, 133);
            this.modAuthor.Name = "modAuthor";
            this.modAuthor.Size = new System.Drawing.Size(191, 20);
            this.modAuthor.TabIndex = 3;
            // 
            // modName
            // 
            this.modName.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "modTitle", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.modName.Location = new System.Drawing.Point(85, 50);
            this.modName.Name = "modName";
            this.modName.Size = new System.Drawing.Size(191, 20);
            this.modName.TabIndex = 1;
            this.modName.Text = global::Mod_Starter.Properties.Settings.Default.modTitle;
            this.modName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.LightGreen;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button9.Location = new System.Drawing.Point(1228, 11);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(223, 34);
            this.button9.TabIndex = 23;
            this.button9.Text = "Start  Server";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button26);
            this.tabPage4.Controls.Add(this.button25);
            this.tabPage4.Controls.Add(this.button24);
            this.tabPage4.Controls.Add(this.button23);
            this.tabPage4.Controls.Add(this.button22);
            this.tabPage4.Controls.Add(this.presetName);
            this.tabPage4.Controls.Add(this.presetsList);
            this.tabPage4.Controls.Add(this.RefreshMods);
            this.tabPage4.Controls.Add(this.WorkshopMods);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1474, 574);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Workshop Mods";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(1071, 35);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(75, 23);
            this.button26.TabIndex = 8;
            this.button26.Text = "Exclude";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(994, 35);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(75, 23);
            this.button25.TabIndex = 7;
            this.button25.Text = "Append";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(1148, 35);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(75, 23);
            this.button24.TabIndex = 6;
            this.button24.Text = "Delete";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.GreenYellow;
            this.button23.Location = new System.Drawing.Point(917, 35);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(75, 23);
            this.button23.TabIndex = 5;
            this.button23.Text = "Activate";
            this.button23.UseVisualStyleBackColor = false;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(917, 6);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 4;
            this.button22.Text = "Save Preset";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // presetName
            // 
            this.presetName.Location = new System.Drawing.Point(573, 7);
            this.presetName.Name = "presetName";
            this.presetName.Size = new System.Drawing.Size(337, 20);
            this.presetName.TabIndex = 3;
            // 
            // presetsList
            // 
            this.presetsList.FormattingEnabled = true;
            this.presetsList.Location = new System.Drawing.Point(572, 35);
            this.presetsList.Name = "presetsList";
            this.presetsList.Size = new System.Drawing.Size(338, 21);
            this.presetsList.TabIndex = 2;
            // 
            // RefreshMods
            // 
            this.RefreshMods.Location = new System.Drawing.Point(491, 476);
            this.RefreshMods.Name = "RefreshMods";
            this.RefreshMods.Size = new System.Drawing.Size(75, 23);
            this.RefreshMods.TabIndex = 1;
            this.RefreshMods.Text = "Refresh";
            this.RefreshMods.UseVisualStyleBackColor = true;
            this.RefreshMods.Click += new System.EventHandler(this.RefreshMods_Click);
            // 
            // WorkshopMods
            // 
            this.WorkshopMods.CheckBoxes = true;
            this.WorkshopMods.Location = new System.Drawing.Point(6, 6);
            this.WorkshopMods.Name = "WorkshopMods";
            this.WorkshopMods.Size = new System.Drawing.Size(560, 493);
            this.WorkshopMods.TabIndex = 0;
            this.WorkshopMods.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.WorkshopMods_AfterCheck);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.linkLabel1);
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1474, 574);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Settings Config File";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(259, 305);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(151, 25);
            this.label13.TabIndex = 11;
            this.label13.Text = "WIP do not use.";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(3, 3);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(211, 13);
            this.linkLabel1.TabIndex = 10;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Requires Community Framweork @CF; mod";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(549, 268);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 8;
            this.button6.Text = "ExportXML";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(38, 111);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(578, 150);
            this.dataGridView1.TabIndex = 7;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(775, 72);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 6;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(34, 493);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.ConfigModFolder);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Location = new System.Drawing.Point(6, 29);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(368, 66);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Create config for mod";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(277, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ConfigModFolder
            // 
            this.ConfigModFolder.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Mod_Starter.Properties.Settings.Default, "CfgModFolder", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ConfigModFolder.Location = new System.Drawing.Point(19, 34);
            this.ConfigModFolder.Name = "ConfigModFolder";
            this.ConfigModFolder.Size = new System.Drawing.Size(251, 20);
            this.ConfigModFolder.TabIndex = 2;
            this.ConfigModFolder.Text = global::Mod_Starter.Properties.Settings.Default.CfgModFolder;
            this.ConfigModFolder.TextChanged += new System.EventHandler(this.ConfigModFolder_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Select mod folder";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.textBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1474, 574);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Scratchpad";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(232, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Just a place to take notes. Automatically saved.";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(7, 26);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(680, 371);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // launchMode
            // 
            this.launchMode.Controls.Add(this.clientOnly);
            this.launchMode.Controls.Add(this.ServerOnly);
            this.launchMode.Controls.Add(this.ClientServer);
            this.launchMode.Location = new System.Drawing.Point(905, 770);
            this.launchMode.Name = "launchMode";
            this.launchMode.Size = new System.Drawing.Size(315, 37);
            this.launchMode.TabIndex = 26;
            this.launchMode.Visible = false;
            // 
            // clientOnly
            // 
            this.clientOnly.AutoSize = true;
            this.clientOnly.Checked = global::Mod_Starter.Properties.Settings.Default.ClientOnlyLaunch;
            this.clientOnly.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "ClientOnlyLaunch", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.clientOnly.Location = new System.Drawing.Point(165, 5);
            this.clientOnly.Name = "clientOnly";
            this.clientOnly.Size = new System.Drawing.Size(51, 17);
            this.clientOnly.TabIndex = 27;
            this.clientOnly.Text = "Client";
            this.clientOnly.UseVisualStyleBackColor = true;
            // 
            // ServerOnly
            // 
            this.ServerOnly.AutoSize = true;
            this.ServerOnly.Checked = global::Mod_Starter.Properties.Settings.Default.ServerOnlyLaunch;
            this.ServerOnly.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "ServerOnlyLaunch", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ServerOnly.Location = new System.Drawing.Point(103, 5);
            this.ServerOnly.Name = "ServerOnly";
            this.ServerOnly.Size = new System.Drawing.Size(56, 17);
            this.ServerOnly.TabIndex = 26;
            this.ServerOnly.Text = "Server";
            this.ServerOnly.UseVisualStyleBackColor = true;
            // 
            // ClientServer
            // 
            this.ClientServer.AutoSize = true;
            this.ClientServer.Checked = global::Mod_Starter.Properties.Settings.Default.ServerAndClientLaunch;
            this.ClientServer.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Mod_Starter.Properties.Settings.Default, "ServerAndClientLaunch", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ClientServer.Location = new System.Drawing.Point(0, 6);
            this.ClientServer.Name = "ClientServer";
            this.ClientServer.Size = new System.Drawing.Size(94, 17);
            this.ClientServer.TabIndex = 25;
            this.ClientServer.TabStop = true;
            this.ClientServer.Text = "Server + Client";
            this.ClientServer.UseVisualStyleBackColor = true;
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialog1.HelpRequest += new System.EventHandler(this.folderBrowserDialog1_HelpRequest);
            // 
            // BrowseP
            // 
            this.BrowseP.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.BrowseP.SelectedPath = "P:\\";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            // 
            // ConsoleLog
            // 
            this.ConsoleLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ConsoleLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConsoleLog.Location = new System.Drawing.Point(12, 610);
            this.ConsoleLog.Multiline = true;
            this.ConsoleLog.Name = "ConsoleLog";
            this.ConsoleLog.ReadOnly = true;
            this.ConsoleLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ConsoleLog.Size = new System.Drawing.Size(1238, 250);
            this.ConsoleLog.TabIndex = 1;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 15000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip1.ToolTipTitle = "Additional info";
            // 
            // infoIcon
            // 
            this.infoIcon.AutoSize = true;
            this.infoIcon.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.infoIcon.ForeColor = System.Drawing.Color.Blue;
            this.infoIcon.Location = new System.Drawing.Point(101, 97);
            this.infoIcon.Name = "infoIcon";
            this.infoIcon.Size = new System.Drawing.Size(22, 13);
            this.infoIcon.TabIndex = 38;
            this.infoIcon.Text = "(?)";
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.Color.LimeGreen;
            this.button32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button32.Location = new System.Drawing.Point(1405, 200);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(46, 23);
            this.button32.TabIndex = 49;
            this.button32.Text = "RUN";
            this.button32.UseVisualStyleBackColor = false;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1506, 866);
            this.Controls.Add(this.ConsoleLog);
            this.Controls.Add(this.ModsToLoad);
            this.Controls.Add(this.launchMode);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Mod Starter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ModsToLoad.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.CheckBoxPanel.ResumeLayout(false);
            this.CheckBoxPanel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.launchMode.ResumeLayout(false);
            this.launchMode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl ModsToLoad;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox LanguagaCheckbox;
        private System.Windows.Forms.CheckBox GuiCheckbox;
        private System.Windows.Forms.CheckBox DataCheckbox;
        private System.Windows.Forms.TextBox modPfolder;
        private System.Windows.Forms.RichTextBox modDescription;
        private System.Windows.Forms.TextBox modAuthor;
        private System.Windows.Forms.TextBox modName;
        private System.Windows.Forms.CheckBox MissionCheckbox;
        private System.Windows.Forms.CheckBox WorldCheckbox;
        private System.Windows.Forms.CheckBox GameCheckbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox pboFolderCheckpbox;
        private System.Windows.Forms.CheckBox modcppCheckbox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox playerName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox prefixMods;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox killProcesses;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox ConfigModFolder;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox requiredAddons;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox DiskPpath;
        private System.Windows.Forms.FolderBrowserDialog BrowseP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox DayZPath;
        private System.Windows.Forms.FolderBrowserDialog BrowseDayZ;
        private System.Windows.Forms.CheckBox TypesFolderCheckbox;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button6;
        private System.Data.DataSet dataSet1;
        private System.Windows.Forms.Button createStructure;
        private System.Windows.Forms.Panel CheckBoxPanel;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button packPbo;
        private System.Windows.Forms.TextBox ConsoleLog;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.CheckBox isServerMod;
        private System.Windows.Forms.TextBox serverModPrefix;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button openPackFolder;
        private System.Windows.Forms.Button openpFolder;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox CustomFolder;
        private System.Windows.Forms.ListBox filesInDir;
        private System.Windows.Forms.ListBox pDirectories;
        private System.Windows.Forms.Button CNTcrash;
        private System.Windows.Forms.Button CNTrpt;
        private System.Windows.Forms.Button CNTscript;
        private System.Windows.Forms.Button SRVcrash;
        private System.Windows.Forms.Button SRVrpt;
        private System.Windows.Forms.Button SRVscript;
        private System.Windows.Forms.Button startClientBut;
        private System.Windows.Forms.Panel launchMode;
        private System.Windows.Forms.CheckBox startBothCheckbox;
        private System.Windows.Forms.RadioButton clientOnly;
        private System.Windows.Forms.RadioButton ServerOnly;
        private System.Windows.Forms.RadioButton ClientServer;
        private System.Windows.Forms.Button clearLogs;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.CheckBox packBeforeStart;
        private System.Windows.Forms.CheckBox useServerMods;
        private System.Windows.Forms.Button openAllFiles;
        private System.Windows.Forms.CheckBox FilterCheckbox;
        private System.Windows.Forms.TextBox PboPrefix;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.CheckBox CoreCheckbox;
        private System.Windows.Forms.TextBox ClnScriptLogPath;
        private System.Windows.Forms.TextBox SrvScriptLogPath;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button OpenProfiles;
        private System.Windows.Forms.Button OpenWorkshop;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox LaunchParams;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.TreeView WorkshopMods;
        private System.Windows.Forms.Button RefreshMods;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.TextBox presetName;
        private System.Windows.Forms.ComboBox presetsList;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button WipeMissionDiag;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.TextBox ToolsDir;
        private System.Windows.Forms.Button StartWorkbench;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton Packer_MakePbo;
        private System.Windows.Forms.RadioButton Packer_AddonBuilder;
        private System.Windows.Forms.RadioButton Packer_PboProject;
        private System.Windows.Forms.Button CreateServerBat;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.TextBox ServerDir;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label infoIcon;
        private System.Windows.Forms.Button button32;
    }
}

