<b>Before launching</b>
Install DayZ Tools - https://store.steampowered.com/app/830640/DayZ_Tools/

Instructions: https://www.youtube.com/watch?v=HENAtewJbFU

<b>Known bugs:</b>
- Many errors can be shown on startup due to old paths were saved when compiling. Just set correct paths and restart.